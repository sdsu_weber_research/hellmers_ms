\appendices
%
% If you only have one appendix, you should change the above to:
%\appendix
%

\chapter{PYCNOCALC}

We use the Fortran application PycnoCalc to perform all calculations for the pycnonuclear reaction rates in this thesis.
The complete source code is available in the Bitbucket repository at \url{https://bitbucket.org/sdsu_weber_research/pycnocalc/src/master}.
PycnoCalc was originally developed for the author's Physics Bachelor of Science degree thesis in 2008, where the routine to calculate the folding potential was originally created.
The folding potential routine of PycnoCalc was used for Master of Science theses of Golf and Ryan (n\'ee Currier) wherein pycnonuclear reaction rates were calculated for heavy ions with strange 
quark matter nuggets \cite{GOLFTHESIS,RYANTHESIS}.
This routine was also used to perform calculations reported in the Physics Review C article of Golf, Hellmers and Weber \cite{GOLF2009}.
Subsequent to the completion of those theses the calculations were incorporated into PycnoCalc.
We will now discuss some of the important Fortran modules and how they were used and modified to perform the calculations in this thesis.

\section{\texttt{nucleon\textunderscore interactions}}
This module contains the functions that calculate the M3Y, S\~{a}o Paulo, and RMF nucleon interactions.
The RMF interaction was added specifically for this thesis.  

This function for the RMF reaction supports the HS, Z, W and L1 parameter sets from \cite{SINGH2012,SINGH2012C} even though we only used the L1 parameter set in our calculations.

The $\delta$-function term in the M3Y nucleon-nucleon potential in Equation~(\ref{eqn:m3ypot}) is handled by an input parameter indicating the how close the volume elements of 
nuclear matter are allowed to get before simply adding the value $262$ to the calculated expression.

\section{\texttt{folding\textunderscore potential}}
This module contains the routine that calculates the folding potential.
This routine integrates over the volumes of both the target and the incoming nuclei, yielding a nested for loop of six levels.
It is very slow, however we have kept this structure in order to potentially compute the folding potential for deformed nuclei at a future date.
Thus far attempts to speed this routine up by using \texttt{OpenMP} parallelization have not been successful.
The only modification made for this thesis was to add the ability to use the RMF nucleon-nucleon interaction.

\section{\texttt{rate\textunderscore calc}}

The \texttt{rate\textunderscore calc} module contains functions to calculate the astrophysical S-factor, the SPVH, KS and G05 reaction rates.

\section{\texttt{csv\textunderscore kinds}, \texttt{csv\textunderscore module}, \texttt{csv\textunderscore parameters}, and \texttt{csv\textunderscore utilities}}

These modules are part of a comma-separated-value (CSV) formatted files open source package from Jacob Williams \url{https://github.com/jacobwilliams/fortran-csv-module}.
This package was incorporated into PycnoCalc in order to more efficiently write and read CSV files.
We use the Python package \texttt{pandas} to help with generating plots, as well as do other data analysis for PycnoCalc output, and \texttt{pandas} seamlessly uses CSV files.

\section{\texttt{mathdiff}, \texttt{mathintegration}, \texttt{mathinterpolation}, \texttt{mathlinearalg}, and \texttt{mathutils}}

These modules are primarily based on the algorithms and software described in \cite{NUMERICALRECIPES}.
The routines in these modules are not meant to be comprehensive or even particularly robust, but are implemented for quick, useful routines to perform the basic numerical analysis we need for PycnoCalc.
The module \texttt{mathintegration} implements the trapezoid quadrature method.
One version is based on function values preloaded into an array, and another version has a function name passed in and computes the numeric integral for that function.
These methods are used to calculate the 2pF nuclear density functional and the WKB approximation for the S-factor, respectively.
We implement the creation and the use of cubic splines in \texttt{mathinterpolation}.
This is used to calculate the pycnonuclear rates used in the \texttt{pycno.f90} MESA program unit.
In \texttt{mathlinearalg} we implement the solving of tridiagonal systems of equations.
This is needed in order to compute the cubic spline.
The 5-point numerical differentiation in module \texttt{mathdiff} is calculated as described in \cite{koonin1990computational}.

\section{\texttt{tests} and \texttt{system\textunderscore functions}}

The module \texttt{tests} contains all the unit tests that are done during development.
A configuration file name is passed into PycnoCalc, indicating which unit tests will be performed.
At this time we have no baselines to track against, but we can at least rerun tests with the correct parameters to verify basic functionality.

The module \texttt{system\textunderscore functions} contains all the code that actually produces research results that we are interested in retaining.
For example, the routines that generate the pycnonuclear reaction rates are contained in this module.
When the routines in this module execute, they typically output data into a format that we will use either in another piece of code, like \texttt{pycno.f90}, or to plot or
put that data into a \LaTeX\ table.
Similar to unit tests, there is a configuration file that is used for each function that generates research data.

\chapter{SOME COMPUTATIONAL DETAILS}
\label{appendix:compDetails}
\section{Use of Bitbucket for Source Code Management}

All of the artifacts generated while creating this thesis, except for the raw model data from the models and logs files, are managed in the Bitbucket repository.
Bitbucket provides free source code repositories with certain limitations.
The main limitations are only 5 users for private repositories, and the repository size cannot be more than 1 GB.

Source code management provides many benefits.
One benefit is the ability to have a central location for all elements required to reproduce the results of a particular project, this Master's thesis in this case.
Another benefit is the ability to merged incremental updates of the source code and other artifacts.
This allows for the creation of a consistent baseline at important milestones or task completions.
Very usefully source code repository users can revert to previously known good versions of the application

For larger software projects, using source code management allows multiple software developers to enhance and maintain a software application at the same time.
Typically developers create a branch of the software, and pull the branch onto a computer they use for development.
When completed, they can the push their local changes onto the remote branch in Bitbucket, and then they or another developer can merge those changes into the master branch.
Any conflicts between changes by different developers to the same artifacts can be resolved in this process.

Another major reason to use an online source code repository related to reproducibility is that the repository can act as a permanent record of how the research results were calculated.
It is possible that Bitbucket, which is operated by the company Atlassian, may someday become defunct, but it is also possible to download repositories into a generic \texttt{git} repository
on a file system, and then load it into another source code management system like GitHub or GitLab.

Two sets of artifacts that we are not able to keep in Bitbucket are the raw data for the models and the job output and error logs.
The reason for this is simply because they are too big, taking almost 800 megabytes when compressed.
This amount of data is not really appropriate for a source code management system.
This data is available upon request.

\section{SDSC Used to Generate Models}

All the models list in Tables~\ref{tab:microAssumptions} and~\ref{tab:additionalModels} were run as jobs on the San Diego Supercomputing Center (SDSC) supercomputer Comet.

\begin{table}[hbt]
  \centering
  \begin{minipage}{6.5in}
	\centering
  	\caption{Additional Models Generated\label{tab:additionalModels}}
    \begin{tabular}{|c|c|}    \hline
  		wd\textunderscore ignite\textunderscore nopycno & Accreting white dwarf with no pycnonuclear reactions  \\ \hline
		wd\textunderscore ignite\textunderscore m3y\textunderscore bcc\textunderscore relaxed\textunderscore adj & SK, M3Y, bcc, relaxed with temp. enhancement  \\ \hline
	\end{tabular}
  \end{minipage}
\end{table}

There were two types of jobs that were submitted for this thesis.
The first were the jobs to calculate the pycnonuclear reaction rates.
These jobs were computationally expensive primarily due to the non-optimized, non-parallelized way the double folding-potential is calculated.
There were three long running jobs that ran for nearly 24 hours each, one for each nucleon-nucleon potential.

The second type of job was one which ran the simulation of the accreting white dwarf.
Each type of model had a configuration stored in a folder bearing the model name.
Each job initialized and built the model's software for the run, cleared out data from previous runs, ran the simulation, moved the important model data to a separate folder, and deleted the data in
which we were not interested. 

For the temperature enhanced pycnonuclear reaction model, \texttt{wd\textunderscore ignite\textunderscore m3y\textunderscore bcc\textunderscore relaxed\textunderscore adj}, we were unable to achieve
convergence.
In our efforts to solve the problem we handled this job slightly differently.
For this model we set the number of \texttt{OpenMP} threads to 24 (the maximum allowed on Comet), and set the maximum duration for the job to 48 hours.
This is compared to the rest of the models which achieved convergence relatively quickly (less than an hour) with only 2 threads.


\section{Troubleshooting MESA Convergence Issues}

The models that were initially generated using pycnonuclear reaction rates associated with polarized lattice relaxed condition, or the Wigner-Seitz approximation, never actually reached the 
terminating condition for either energy output or a high enough temperature.
At first we felt this might be due to actual physical differences in the models.
However, it was soon discovered that the reason the models never reached our instability criteria was because the time step used by MESA became very small, less than one year.
Further analysis showed this was due to a large number of Newton-Raphson iterations required to reach stability for some of the shells of the models.
MESA has the characteristic that when the number of these iterations is larger than a certain value (14 by default), the time step will be reduced.
The models that did not have this problem did not have a time step less than around one million years.
In order to identify the zones where the problem was occurring, we used MESA debugging tools.
Using one of these tools we were able to generate a graphic which allowed us to narrow down the issues to a region.

\begin{figure}[!htb]
  \centering
  \begin{minipage}{4.5in}
    \includegraphics[width=\linewidth]{Figures/MESATroubleshooting.eps}
    \caption{The red areas in this troubleshooting graphic indicate areas where the number of Newton-Raphson iterations is over the threshold to decrease the time step size.\label{fig:MESATroubleshooting}}
  \end{minipage}
\end{figure}

Further research indicated that on some noise in some of the pycnonuclear reaction rates, and very low values of those rates, was causing the spline we used to calculate negative reactions rates.
This problem was ultimately resolved by using a threshold below which reaction rates and their associated derivatives would be set to zero.
This threshold is set to $10^{-200}$.

\section{Lack of Convergence for Model Using Temperature Enhancement}

One of the more challenging physical values to calculate is the adjustment of pycnonuclear reactions rates based on temperature.
It is tempting to simply perform the calculations in Equations~(\ref{eqn:excitationparam}),~(\ref{eqn:tempadjust1}), and~(\ref{eqn:tempadjust2}).
However, as shown in Figures~\ref{fig:tempAdjustSurfStatic} and~\ref{fig:tempAdjustSurfRelaxed}, there are features at the lower densities that are rather nonphysical
In order to deal with this situation, we have modified our implementation to only perform the temperature adjustment calculation for a relatively narrow range of densities and temperatures.
However, there was never enough numerical stability to allow the model to achieve the normal temperature or nuclear energy stopping conditions.

\begin{figure}[!htb]
  \centering
  \begin{minipage}{4.5in}
    \includegraphics[width=\linewidth]{Figures/tempAdjustSurfStatic.eps}
    \caption{The Log of the temperature adjustment when using the static lattice approximation.\label{fig:tempAdjustSurfStatic}}
  \end{minipage}
\end{figure}

\begin{figure}[!htb]
  \centering
  \begin{minipage}{4.5in}
    \includegraphics[width=\linewidth]{Figures/tempAdjustSurfRelaxed.eps}
    \caption{The Log of the temperature adjustment when using the relaxed lattice approximation.\label{fig:tempAdjustSurfRelaxed}}
  \end{minipage}
\end{figure}

\chapter{PERCENT ERROR FROM MEAN OF OBSERVATIONAL PROPERTIES}
\label{appendix:errorsFromMean}
Tables~\ref{tab:radiiErrors} and~\ref{tab:teffErrors} show percentage deviations from the mean of the final radii and effective temperatures respectively for all of our different pycnonuclear rate calculations.
We have included negative percentages to indicate that the value is lower than the mean value.
In all cases the variation in expect values are less than 1\% and are not likely to be observationally detectable.


\begin{table}[hbt]
  \centering
  \begin{minipage}{5.5in}
    \centering
    \caption{Percent Error from Mean of Final Radii\label{tab:radiiErrors}.}
    \begin{tabular}{|c|c|c|c||c|c|}    \hline
      Calc.Type & NN-Pot. & Cell & Cell Approx. & R (km) & \% Error from Mean \\ \hline \hline
	No Pycno & - & - & - & 1505 & - \\ \hline
	G05 & S\~{a}o Paulo & bcc & Static & 1340 & -0.36 \\ \hline
	SPVH & M3Y & bcc & Static & 1339 & -0.42 \\ \hline
	SK & M3Y & bcc & Relaxed & 1351 & 0.51 \\ \hline
	SK & M3Y & bcc & Static & 1339 & -0.42 \\ \hline
	SK & M3Y & bcc & WS & 1352 & 0.55 \\ \hline
	SK & M3Y & fcc & Relaxed & 1347 & 0.17 \\ \hline
	SK & M3Y & fcc & Static & 1337 & -0.55 \\ \hline
	SK & M3Y & fcc & WS & 1347 & 0.20 \\ \hline
	SPVH & Sao Paulo & bcc & Static & 1338 & -0.49 \\ \hline
	SK & Sao Paulo & bcc & Relaxed & 1349 & 0.34 \\ \hline
	SK & Sao Paulo & bcc & Static & 1337 & -0.58 \\ \hline
	SK & Sao Paulo & bcc & WS & 1349 & 0.37 \\ \hline
	SK & Sao Paulo & fcc & Relaxed & 1347 & 0.17 \\ \hline
	SK & Sao Paulo & fcc & Static & 1338 & -0.50 \\ \hline
	SK & Sao Paulo & fcc & WS & 1344 & -0.02 \\ \hline
	SPVH & RMF & bcc & Static & 1339 & -0.42 \\ \hline
	SK & RMF & bcc & Relaxed & 1354 & 0.69 \\ \hline
	SK & RMF & bcc & Static & 1338 & -0.49 \\ \hline
	SK & RMF & bcc & WS & 1354 & 0.73 \\ \hline
	SK & RMF & fcc & Relaxed & 1351 & 0.52 \\ \hline
	SK & RMF & fcc & Static & 1338 & -0.44 \\ \hline
	SK & RMF & fcc & WS & 1350 & 0.43 \\ \hline
	\end{tabular}
  \end{minipage}
\end{table}

\begin{table}[hbt]
  \centering
  \begin{minipage}{5.5in}
    \centering
    \caption{Percent Error from Mean of Final Effective Temperatures\label{tab:teffErrors}.}
    \begin{tabular}{|c|c|c|c||c|c|}    \hline
      Calc.Type & NN-Pot. & Cell & Cell Approx. & Teff (K) & \% Error from Mean \\ \hline \hline
		No Pycno & - & - & - & 96153 & - \\ \hline
		G05 & S\~{a}o Paulo & bcc & Static & 106612 & 0.24 \\ \hline
		SPVH & M3Y & bcc & Static & 106627 & 0.25 \\ \hline
		SK & M3Y & bcc & Relaxed & 105836 & -0.49 \\ \hline
		SK & M3Y & bcc & Static & 106717 & 0.34 \\ \hline
		SK & M3Y & bcc & WS & 105910 & -0.42 \\ \hline
		SK & M3Y & fcc & Relaxed & 106225 & -0.13 \\ \hline
		SK & M3Y & fcc & Static & 106859 & 0.47 \\ \hline
		SK & M3Y & fcc & WS & 106138 & -0.21 \\ \hline
		SPVH & Sao Paulo & bcc & Static & 106759 & 0.38 \\ \hline
		SK & Sao Paulo & bcc & Relaxed & 106016 & -0.32 \\ \hline
		SK & Sao Paulo & bcc & Static & 106849 & 0.46 \\ \hline
		SK & Sao Paulo & bcc & WS & 106086 & -0.26 \\ \hline
		SK & Sao Paulo & fcc & Relaxed & 106223 & -0.13 \\ \hline
		SK & Sao Paulo & fcc & Static & 106748 & 0.37 \\ \hline
		SK & Sao Paulo & fcc & WS & 106386 & 0.02 \\ \hline
		SPVH & RMF & bcc & Static & 106692 & 0.31 \\ \hline
		SK & RMF & bcc & Relaxed & 105839 & -0.49 \\ \hline
		SK & RMF & bcc & Static & 106926 & 0.53 \\ \hline
		SK & RMF & bcc & WS & 105880 & -0.45 \\ \hline
		SK & RMF & fcc & Relaxed & 105913 & -0.42 \\ \hline
		SK & RMF & fcc & Static & 106749 & 0.37 \\ \hline
		SK & RMF & fcc & WS & 105925 & -0.41 \\ \hline
	\end{tabular}
  \end{minipage}
\end{table}

\chapter{CONTRIBUTORS FOR SOURCE CODE}
\label{appendix:contributions}

The original folding potential calculation software was written by myself for my Bachelor's of Science in Physics thesis from 2008.

Barbara Golf used this folding potential software as part of the calculation of the S-factor, and used that to calculate pycnonuclear reactions rates
for nuclei and strange quark matter in her thesis.
Whitney Ryan added the M3Y nucleon-nucleon interaction.
These capabilities were then retrofitted into \texttt{PycnoCalc} by me.

For this thesis I did the following:

\begin{description}[font=$\bullet$~\normalfont\scshape\color{red!50!black}]
\item Added the RMF nucleon-nucleon interaction
\item Added the temperature enhancements
\item Added the Schramm-Koonin formulation for reaction rates
\item Modified MESA to use my pycnonuclear reaction rates
\item Used Python Matplotlib and Pandas to produce charts
\end{description}



