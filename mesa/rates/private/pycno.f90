! ***********************************************************************
!
!   Copyright (C) 2010  Bill Paxton
!
!   MESA is free software; you can use it and/or modify
!   it under the combined terms and restrictions of the MESA MANIFESTO
!   and the GNU General Library Public License as published
!   by the Free Software Foundation; either version 2 of the License,
!   or (at your option) any later version.
!
!   You should have received a copy of the MESA MANIFESTO along with
!   this software; if not, it is available at the mesa website:
!   http://mesa.sourceforge.net/
!
!   MESA is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
!   See the GNU Library General Public License for more details.
!
!   You should have received a copy of the GNU Library General Public License
!   along with this software; if not, write to the Free Software
!   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
!
! ***********************************************************************


! The following modules/functions were pulled from pycnocalc.  If there are changed in pycnocalc they need to be updated here as well.
!
!   astrophysics/inv_len_param2comp
!   astrophysics/mean_wt_electron2comp
!   astrophysics/beta_excitation2comp
!   rate_calc/tempRateAdjust
!   mathutils/locate
!   mathdiff/ndiff5pt
!
!   Last Check: 10/14/2019
!


      module pycno
      use rates_def
      use utils_lib
      use const_def, only: dp
      use crlibm_lib
      
      implicit none
      
      ! Determine if the type of Pycno Rxn is set already
      ! Note that this flag is in a separate memory space for each OpenMP thread
      logical :: setFlag = .false.
      
      integer :: nnIntType
      ! 0 - Use OOTB G05 rates
      ! 1 - Sao Paulo
      ! 2 - M3Y
      ! 3 - RMF

      integer :: pycnoType
      ! 0 - SPVH
      ! 1 - bcc static
      ! 2 - bcc ws
      ! 3 - bcc relaxed
      ! 4 - fcc static
      ! 5 - fcc ws
      ! 6 - fcc relaxed
      ! 9 - don't include carbon-carbon pycnonuclear reaction rates       

      contains
      
      subroutine G05_epsnuc_CC(T, Rho, X12, eps, deps_dT, deps_dRho)
         
         ! from Gasques, et al, Nuclear fusion in dense matter.  astro-ph/0506386.
         ! Phys Review C, 72, 025806 (2005)
         
         use const_def
         
         real(dp), intent(in) :: T
         real(dp), intent(in) :: Rho
         real(dp), intent(in) :: X12 ! mass fraction of c12
         real(dp), intent(out) :: eps ! rate in ergs/g/sec
         real(dp), intent(out) :: deps_dT ! partial wrt temperature
         real(dp), intent(out) :: deps_dRho ! partial wrt density
         
         
         real(dp), parameter :: exp_cutoff = 200d0
         real(dp), parameter :: exp_max_result = 1d200
         real(dp), parameter :: exp_min_result = 1d-200

         real(dp), parameter :: sqrt3 = 1.73205080756888d0 ! sqrt[3]
         
         real(dp), parameter :: Z = 6 ! charge of C12
         real(dp), parameter :: A = 12 ! atomic number for C12

         real(dp), parameter :: b_ne20 = 160.64788d0
         real(dp), parameter :: b_c12  =  92.1624d0
         real(dp), parameter :: b_he4  =  28.2928d0
         
         ! coefficients from "optimal" model 1
         real(dp), parameter :: Cexp = 2.638
         real(dp), parameter :: Cpyc = 3.90
         real(dp), parameter :: Cpl = 1.25
         real(dp), parameter :: CT = 0.724
         real(dp), parameter :: Csc = 1.0754
         real(dp), parameter :: Lambda = 0.5  
         
         real(dp) :: m, Zqe, Z2e2, Q1212, MeV_to_erg, barn_to_cm2, MeV_barn_to_erg_cm2, ergs_c12c12
         
         real(dp) :: Tk, ni, a_ion, Gamma, omega_p, Tp
         real(dp) :: Ea, tau, Epk, SEpk, lam, Rpyc, Ppyc, Fpyc
         real(dp) :: Ttilda, Gammatilda, tautilda, phi, gam1, gam2, gam
         real(dp) :: P, lnF, F, deltaR, R

         real(dp) :: Epk1, Epk2x, SEpkx1, SEpkx2x, SEpkx2d, SEpkx2n, SEpkx2, phin, phid
         real(dp) :: Epk2c, dEpk1_dT, dni_dRho, dTk_dT, da_ion_dRho, dGamma_dT, &
               dGamma_dRho, domega_p_dRho, dTp_dRho
         real(dp) :: dtau_dT, dEpk1_dRho, dEpk2x_dT, dEpk2x_dRho, dEpk2c_dT, dEpk2c_dRho, dEpk_dT, dEpk_dRho
         real(dp) :: dSEpkx1_dT, dSEpkx1_dRho, dSEpkx2x_dT, dSEpkx2x_dRho, dSEpkx2d_dT, dSEpkx2d_dRho
         real(dp) :: dSEpkx2n_dT, dSEpkx2n_dRho, dSEpkx2_dT, dSEpkx2_dRho, dSEpk_dT, dSEpk_dRho
         real(dp) :: dlam_dRho, dPpyc_dRho, dFpyc_dRho, dRpyc_dT, dRpyc_dRho, gam_n, gam_d, dgam_dT, dgam_dRho
         real(dp) :: dphin_dGamma, dphin_dT, dphin_dRho, dphid_dGamma,  &
               dphid_dT, dphid_dRho, dphidT, dphidRho
         real(dp) :: dTtilda_dT, dTtilda_dRho, dGammatilda_dT, dGammatilda_dRho,  &
               dtautilda_dTtilda, dtautilda_dT, dtautilda_dRho
         real(dp) :: dgam_n_dT, dgam_n_dRho, dgam_d_dT, dgam_d_dRho, dP_dgam, dP_dTtilda, dP_dT, dP_dRho
         real(dp) :: lnF1, lnF2x, lnF2, lnF3, dlnF1_dT, dlnF1_dRho, dlnF2x_dT, dlnF2x_dRho, dlnF2_dT, dlnF2_dRho
         real(dp) :: dlnF3_dT, dlnF3_dRho, dlnF_dT, dlnF_dRho, dF_dT, dF_dRho, niterm, dniterm_dRho
         real(dp) :: d_deltaR_dT, d_deltaR_dRho, dRdT, dRdRho, exponent
                   
         integer  :: status, pwdlen
         integer  :: unit = 45
         character(len=128) :: pwd, pycnoTypeFile
         real(dp) :: tempAdjust
         logical  :: staticFlag = .true.
         integer  :: tempAdjustIndicator = 0

         real(kind=dp)  :: f1Rho, f2Rho, f3Rho, f4Rho
         real(kind=dp)  :: hRho, hTemp
         real(kind=dp)  :: f1AdjustTemp, f2AdjustTemp, f3AdjustTemp, f4AdjustTemp
         real(kind=dp)  :: f1AdjustRho, f2AdjustRho, f3AdjustRho, f4AdjustRho
         real(kind=dp)  :: x, ndiff
         
         !real(dp) :: maxTempAdjust = 0.0_dp

         include 'formats.dek'
         
         if (.not. setFlag) then
            call get_environment_variable('PWD',pwd, pwdlen, status)
            pycnoTypeFile = trim(pwd) // "/pycnotype.dat"
            print *,"PWD: ", pwd
            print *,"Pycno Type File:", pycnoTypeFile
            pycnoType = 0
            open (unit,file=pycnoTypeFile, status='OLD',action='READ', iostat=status)
            if (status .EQ. 0) then
               readloop: Do
                  ! TODO: When running with more than one OMP thread, this read gives an error
                  !       Need to figure out how to read it without sharing
                  read(unit,*,iostat=status) nnIntType, pycnoType, tempAdjustIndicator
                  if (status .EQ. -1) then
                     ! End of file
                     exit
                  end if
                  if (status .GT. 0) then
                     print *, "Error Reading Pycno Type file", status
                  end if
               end do readloop
            end if
            close (unit)

            print *,"NN interaction type ", nnIntType
            print *,"Pycno type ", pycnoType
            print *,"Pycno tempAdjustIndicator:", tempAdjustIndicator
            if (pycnoType .eq. 3 .or. pycnoType .eq. 4 .or. pycnoType .eq. 5 .or. pycnoType .eq. 6) then
               staticFlag = .false.
            end if
            setFlag = .true.
         end if
         
         
         m = A * amu  ! ion mass of C12 = 12 amu by definition
         Zqe = Z*qe
         Z2e2 = Zqe*Zqe
         Q1212 = b_ne20 + b_he4 - 2 * b_c12 ! MeV
         MeV_to_erg = 1d6 * ev2erg
         barn_to_cm2 = 1d-24
         MeV_barn_to_erg_cm2 = MeV_to_erg * barn_to_cm2
         ergs_c12c12 = MeV_to_erg * Q1212

         Tk = T * kerg ! temperature in ergs
         dTk_dT = kerg

         ! number density of ions, cm^-3
         ! this entire routine gets the rate assuming pure 12C
         ni = Rho * avo / A
         dni_dRho = avo / A
         
         ! section III.A.
         a_ion = pow_cr(3 / (4 * Pi * ni), one_third) ! ion-sphere radius, cm
         da_ion_dRho = -one_third * a_ion * dni_dRho / ni
         
         Gamma = Z2e2 / (a_ion * Tk) ! Coulomb coupling parameter
         dGamma_dT = - Gamma * dTk_dT / Tk
         dGamma_dRho =  - Gamma * da_ion_dRho / a_ion
         
         omega_p = DSQRT(4 * Pi * Z2e2 * ni / m) ! ion plasma frequency
         domega_p_dRho = 0.5d0 * omega_p * dni_dRho / ni
         
         Tp = hbar * omega_p ! ion plasma temperature
         dTp_dRho = hbar * domega_p_dRho
         
         ! section III.B.
         Ea = m * (Z2e2 / hbar)**2 ! (eqn 16)
         
         tau = 3 * pow_cr(Pi/2,two_thirds) * pow_cr(Ea/Tk, one_third) ! (eqn 16)
         dtau_dT = -one_third * tau * dTk_dT / Tk
         
         Epk1 = hbar * omega_p
         dEpk1_dRho = hbar * domega_p_dRho
         dEpk1_dT = 0
         
         exponent = -Lambda*Tp/Tk
         if (exponent < -exp_cutoff) then
            Epk2x = exp_min_result
            dEpk2x_dT = 0
            dEpk2x_dRho = 0
         else if (exponent > exp_cutoff) then
            Epk2x = exp_max_result
            dEpk2x_dT = 0
            dEpk2x_dRho = 0
         else
            Epk2x = exp_cr(exponent)
            dEpk2x_dT = Epk2x * Lambda * Tp * dTk_dT / (Tk*Tk)
            dEpk2x_dRho = -Epk2x * Lambda * Tp * dTp_dRho / Tk
         end if
         
         Epk2c = Z2e2/a_ion + Tk*tau/3
         dEpk2c_dT = (dTk_dT*tau + Tk*dtau_dT)/3
         dEpk2c_dRho = -Z2e2*da_ion_dRho/(a_ion*a_ion)
         
         Epk = (Epk1 + Epk2c * Epk2x) / MeV_to_erg ! (eqn 32)
         dEpk_dT = (dEpk1_dT + dEpk2c_dT * Epk2x + Epk2c * dEpk2x_dT) / MeV_to_erg
         dEpk_dRho = (dEpk1_dRho + dEpk2c_dRho * Epk2x + Epk2c * dEpk2x_dRho) / MeV_to_erg

         SEpkx1 = -0.428*Epk
         dSEpkx1_dT = -0.428*dEpk_dT
         dSEpkx1_dRho = -0.428*dEpk_dRho
         
         SEpkx2x = 0.613*(8-Epk)
         dSEpkx2x_dT = -0.613*dEpk_dT
         dSEpkx2x_dRho = -0.613*dEpk_dRho
         
         if (SEpkx2x < -exp_cutoff) then
            SEpkx2d = 1 + exp_min_result
            dSEpkx2d_dT = 0
            dSEpkx2d_dRho = 0
         else if (SEpkx2x > exp_cutoff) then
            SEpkx2d = 1 + exp_max_result
            dSEpkx2d_dT = 0
            dSEpkx2d_dRho = 0
         else
            SEpkx2d = 1 + exp_cr(SEpkx2x)
            dSEpkx2d_dT = (SEpkx2d-1)*dSEpkx2x_dT
            dSEpkx2d_dRho = (SEpkx2d-1)*dSEpkx2x_dRho
         end if
         
         SEpkx2n = 3 * pow_cr(Epk,0.308d0)
         dSEpkx2n_dT = 3 * 0.308 * dEpk_dT / Epk
         dSEpkx2n_dRho = 3 * 0.308 * dEpk_dRho / Epk
         
         SEpkx2 = SEpkx2n / SEpkx2d
         dSEpkx2_dT = (dSEpkx2n_dT - SEpkx2n * dSEpkx2d_dT / SEpkx2d) / SEpkx2d
         dSEpkx2_dRho = (dSEpkx2n_dRho - SEpkx2n * dSEpkx2d_dRho / SEpkx2d) / SEpkx2d
         
         exponent = SEpkx1+SEpkx2
         if (exponent < -exp_cutoff) then
            SEpk = 5.15d16 * exp_min_result
            dSEpk_dT = 0
            dSEpk_dRho = 0
         else if (exponent > exp_cutoff) then
            SEpk = 5.15d16 * exp_max_result
            dSEpk_dT = 0
            dSEpk_dRho = 0
         else
            SEpk = 5.15d16 * exp_cr(exponent) ! (eqn 12)
            dSEpk_dT = SEpk * (dSEpkx1_dT + dSEpkx2_dT)
            dSEpk_dRho = SEpk * (dSEpkx1_dRho + dSEpkx2_dRho)
         end if

         ! section III.D.
         lam = pow_cr(Rho/(1.3574d11*A),one_third) / (A*Z*Z) ! (eqn 24)
         dlam_dRho = one_third * lam / Rho
         
         Ppyc = 8*Cpyc*11.515/pow_cr(lam,Cpl) ! (eqn 23)
         dPpyc_dRho = - Ppyc * dlam_dRho / lam
         
         Fpyc = exp_cr(-Cexp/DSQRT(lam)) ! (eqn 23)
         dFpyc_dRho = Fpyc * Cexp * dlam_dRho / (2 * lam*sqrt(lam))
         
      if (pycnoType .eq. 9) then
         Rpyc = 0
         dRpyc_dT = 0
         dRpyc_dRho = 0
      else        
         
         ! This is code to replace the Pycnonuclear Reaction calculated by this function with my reaction rates
         !print *,"G05 Rate ", Rpyc
         hRho = Rho/(10.0_dp**3)
         select case  (nnIntType)
            case (0)
               Rpyc = Rho * A * pow4(Z) * (1 / (8 * 11.515)) * 1d46 * pow3(lam) * SEpk * Fpyc * Ppyc ! (eqn 25)
            case (1)
               Rpyc = SaoPaulo_Rate(Rho, pycnoType)
               f1Rho = SaoPaulo_Rate(Rho - 2.0_dp*hRho, pycnoType)
               f2Rho = SaoPaulo_Rate(Rho - hRho, pycnoType)
               f3Rho = SaoPaulo_Rate(Rho + hRho, pycnoType)
               f4Rho = SaoPaulo_Rate(Rho + 2.0_dp*hRho, pycnoType)
            case (2)
               Rpyc = M3Y_Rate(Rho, pycnoType)
               f1Rho = M3Y_Rate(Rho - 2.0_dp*hRho, pycnoType)
               f2Rho = M3Y_Rate(Rho - hRho, pycnoType)
               f3Rho = M3Y_Rate(Rho + hRho, pycnoType)
               f4Rho = M3Y_Rate(Rho + 2.0_dp*hRho, pycnoType)
            case (3)
               Rpyc = RMF_Rate(Rho, pycnoType)
               f1Rho = RMF_Rate(Rho - 2.0_dp*hRho, pycnoType)
               f2Rho = RMF_Rate(Rho - hRho, pycnoType)
               f3Rho = RMF_Rate(Rho + hRho, pycnoType)
               f4Rho = RMF_Rate(Rho + 2.0_dp*hRho, pycnoType)
         end select
         if (nnIntType .ne. 0) then
            hTemp = Rpyc/(10.0_dp**3)             
            if ((tempAdjustIndicator .eq. 1) .and. (T .gt. 1.0d8) .and. (Rho .gt. 1.0d9)) then
               tempAdjust = tempRateAdjust(A, Z, 0.5_dp, A, Z, 0.5_dp, Rho, T, .false.)
               f1AdjustRho = tempRateAdjust(A, Z, 0.5_dp, A, Z, 0.5_dp, Rho - 2.0_dp*hRho, T, .false.)
               f2AdjustRho = tempRateAdjust(A, Z, 0.5_dp, A, Z, 0.5_dp, Rho - hRho, T, .false.)
               f3AdjustRho = tempRateAdjust(A, Z, 0.5_dp, A, Z, 0.5_dp, Rho + hRho, T, .false.)
               f4AdjustRho = tempRateAdjust(A, Z, 0.5_dp, A, Z, 0.5_dp, Rho + 2.0_dp*hRho, T, .false.)
               f1AdjustTemp = tempRateAdjust(A, Z, 0.5_dp, A, Z, 0.5_dp, Rho, T - 2.0*hTemp, .false.)
               f2AdjustTemp = tempRateAdjust(A, Z, 0.5_dp, A, Z, 0.5_dp, Rho, T - hTemp, .false.)
               f3AdjustTemp = tempRateAdjust(A, Z, 0.5_dp, A, Z, 0.5_dp, Rho, T + hTemp, .false.)
               f4AdjustTemp = tempRateAdjust(A, Z, 0.5_dp, A, Z, 0.5_dp, Rho, T + 2.0*hTemp, .false.)
               Rpyc = Rpyc * tempAdjust
               if (Rpyc < exp_min_result) then
                  Rpyc = 0
                  dRpyc_dT = 0
                  dRpyc_dRho = 0
               else
                  dRpyc_dT = ndiff5pt(f1Rho*f1AdjustRho, f2Rho*f2AdjustRho, f3Rho*f3AdjustRho, f4Rho*f4AdjustRho, hRho)
                  dRpyc_dRho = ndiff5pt(f1Rho*f1AdjustTemp, f2Rho*f2AdjustTemp, f3Rho*f3AdjustTemp, f4Rho*f4AdjustTemp, hTemp)
               end if
            else
               if (Rpyc < exp_min_result) then
                  Rpyc = 0
                  dRpyc_dT = 0
                  dRpyc_dRho = 0
               else
                  dRpyc_dT = 0.0_dp
                  dRpyc_dRho = ndiff5pt(f1Rho, f2Rho, f3Rho, f4Rho, hRho)
               end if
            end if
         else
            if (Rpyc < exp_min_result) then
               Rpyc = 0
               dRpyc_dT = 0
               dRpyc_dRho = 0
            else
               dRpyc_dT = Rpyc * (dSEpk_dT / SEpk)
               dRpyc_dRho = Rpyc * (3 * dlam_dRho / lam + dSEpk_dRho / SEpk + dFpyc_dRho / Fpyc + dPpyc_dRho / Ppyc)
            end if
         end if
      end if

         ! section III.G.
         phin = DSQRT(Gamma)
         dphin_dGamma = 0.5 * phin / Gamma
         dphin_dT = dGamma_dT * dphin_dGamma
         dphin_dRho = dGamma_dRho * dphin_dGamma
         
         phid = pow_cr(Csc**4 / 9 + Gamma**2, 0.25d0)
         dphid_dGamma = 0.5d0 * Gamma / pow3(phid)
         dphid_dT = dGamma_dT * dphid_dGamma
         dphid_dRho = dGamma_dRho *dphid_dGamma
         
         phi = phin / phid
         dphidT = (dphin_dT - dphid_dT * phin / phid) / phid
         dphidRho = (dphin_dRho - dphid_dRho * phin / phid) / phid
         
         Ttilda = DSQRT(Tk*Tk + (CT*Tp)*(CT*Tp)) ! (eqn 29)
         dTtilda_dT = Tk * dTk_dT / Ttilda
         dTtilda_dRho = CT*CT * Tp * dTp_dRho / Ttilda
         
         Gammatilda = Z2e2 / (a_ion * Ttilda) ! (eqn 29)
         dGammatilda_dT = -Gammatilda * dTtilda_dT / Ttilda
         dGammatilda_dRho = -Gammatilda * (dTtilda_dRho / Ttilda + da_ion_dRho / a_ion)
         
         tautilda = 3 * pow_cr(Pi/2,two_thirds) * pow_cr(Ea / Ttilda, one_third) ! (eqn 29)
         dtautilda_dTtilda = - tautilda / (3 * Ttilda)
         dtautilda_dT = dtautilda_dTtilda * dTtilda_dT
         dtautilda_dRho = dtautilda_dTtilda * dTtilda_dRho
         
         gam1 = two_thirds
         gam2 = two_thirds * (Cpl + 0.5)
         gam_n = Tk*Tk * gam1 + Tp*Tp * gam2
         dgam_n_dT = 2 * Tk * dTk_dT * gam1
         dgam_n_dRho = 2 * Tp * dTp_dRho * gam2
         
         gam_d = Tk*Tk + Tp*Tp
         dgam_d_dT = 2 * Tk * dTk_dT
         dgam_d_dRho = 2 * Tp * dTp_dRho

         gam = gam_n / gam_d ! (eqn 31)
         dgam_dT = (dgam_n_dT - gam_n * dgam_d_dT / gam_d)/gam_d
         dgam_dRho = (dgam_n_dRho - gam_n * dgam_d_dRho / gam_d)/gam_d
         
         P = (8 * pow_cr(Pi/2,one_third)/sqrt3) * pow_cr(Ea/Ttilda, gam) ! (eqn 29)
         if (P < exp_min_result) then
            P = 0
            dP_dgam = 0
            dP_dTtilda = 0
            dP_dT = 0
            dP_dRho = 0
         else
            dP_dgam = P * log_cr(Ea / Ttilda)
            dP_dTtilda = -P * gam / Ttilda
            dP_dT = dP_dgam * dgam_dT + dP_dTtilda * dTtilda_dT
            dP_dRho = dP_dgam * dgam_dRho + dP_dTtilda * dTtilda_dRho
         end if
         
         lnF1 = -tautilda
         exponent = -Lambda * Tp / Tk
         if (exponent < -exp_cutoff) then
            lnF2x = exp_min_result
            dlnF2x_dT = 0
            dlnF2x_dRho = 0
         else if (exponent > exp_cutoff) then
            lnF2x = exp_max_result
            dlnF2x_dT = 0
            dlnF2x_dRho = 0
         else
            lnF2x = exp_cr(exponent)
            dlnF2x_dT = lnF2x * Lambda * Tp * dTk_dT / Tk**2
            dlnF2x_dRho = -lnF2x * Lambda * dTp_dRho / Tk
         end if
         
         lnF2 = Csc * Gammatilda * phi * lnF2x
         lnF3 = - Lambda * Tp / Tk
         lnF = lnF1 + lnF2 + lnF3 ! (eqn 29)
         if (lnF < -exp_cutoff) then
            F = exp_min_result
            dF_dT = 0
            dF_dRho = 0
         else if (lnF > exp_cutoff) then
            F = exp_max_result
            dF_dT = 0
            dF_dRho = 0
         else
            F = exp_cr(lnF)
            dlnF1_dT = -dtautilda_dT
            dlnF1_dRho = -dtautilda_dRho
            dlnF2_dT = lnF2 * (dGammatilda_dT / Gammatilda + dphidT / phi + dlnF2x_dT / lnF2x)
            dlnF2_dRho = lnF2 * (dGammatilda_dRho / Gammatilda + dphidRho / phi + dlnF2x_dRho / lnF2x)
            dlnF3_dT = -lnF3 * dTk_dT / Tk
            dlnF3_dRho = lnF3 * dTp_dRho / Tp
            dlnF_dT = dlnF1_dT + dlnF2_dT + dlnF3_dT
            dlnF_dRho = dlnF1_dRho + dlnF2_dRho + dlnF3_dRho
            dF_dT = dlnF_dT * F
            dF_dRho = dlnF_dRho * F
         end if

         ! NOTE: when calculating deltaR we need to convert SEpk to erg cm^2 from MeV barn.
         ! That conversion is included in the numerical coefficient of the eqn for Rpyc.
         
         niterm = ni*ni * (hbar / (2 * m * Z2e2))
         dniterm_dRho = 2 * niterm * dni_dRho / ni
         
         deltaR = niterm * SEpk * MeV_barn_to_erg_cm2 * P * F ! (eqn 29)
         if (deltaR < 1d-100) then
            deltaR = 0
            d_deltaR_dT = 0
            d_deltaR_dRho = 0
         else
            d_deltaR_dT = deltaR * (dSEpk_dT / SEpk + dP_dT / P + dF_dT / F)
            d_deltaR_dRho = deltaR * (dniterm_dRho / niterm + dSEpk_dRho / SEpk + dP_dRho / P + dF_dRho / F)
         end if
         
         R = X12 * (Rpyc + deltaR) ! eqn 28, s^-1 cm^-3
         dRdT = X12 * (dRpyc_dT + d_deltaR_dT)
         dRdRho = X12 * (dRpyc_dRho + d_deltaR_dRho)

         eps = R * ergs_c12c12 / Rho ! ergs/g/sec
         deps_dT = dRdT * ergs_c12c12 / Rho
         deps_dRho = (dRdRho * ergs_c12c12 - eps) / Rho
      
         if (.true. .or. Rho < 1.7d10) return
         write(*,1) 'T', T
         write(*,1) 'Rho', Rho
         write(*,1) 'X12', X12
         write(*,1) 'eps', eps
         write(*,1) 'deps_dT', deps_dT
         write(*,1) 'deps_dRho', deps_dRho
         write(*,*) 
   
      end subroutine G05_epsnuc_CC
      
      subroutine FL_epsnuc_3alf(T, Rho, Y, UE, r, drdT, drdRho)       
         real(dp), intent(in) :: T ! temperature
         real(dp), intent(in) :: Rho ! density
         real(dp), intent(in) :: Y ! helium mass fraction
         real(dp), intent(in) :: UE ! electron molecular weight
         real(dp), intent(out) :: r ! rate in ergs/g/sec
         real(dp), intent(out) :: drdT ! partial wrt temperature
         real(dp), intent(out) :: drdRho ! partial wrt density


         real(dp) :: T6, R6, R6T, R6T13, R6T16, T62, T612, T623, T653, T632, T613, U, AF
         real(dp) :: G1, dG1dRho, dG1dT, G2, dG2dRho, dG2dT
         real(dp) :: B1, dB1dRho, dB1dT, B2, dB2dRho, dB2dT
         real(dp) :: dUdT, dUdRho, U32, U52, dAFdT, dAFdRho
         real(dp) :: E1, dE1dT, dE1dRho, E2, dE2dT, dE2dRho
         real(dp) :: F1, dF1dT, dF1dRho, F2, dF2dT, dF2dRho
         real(dp) :: dR6dRho, dR6TdRho, dR6T13dRho, dR6T16dRho
         real(dp) :: dT6dT, dT612dT, dT62dT, dT613dT, dT623dT, dT632dT, dT653dT
         
         ! DEBUG
         real(dp), parameter :: AF_0  =  1.9005324047511074D+00
         real(dp), parameter :: B1_denom_0  =  2.9602238143383192D-01
         real(dp), parameter :: B1_0  =  1.2227955158250397D-08
         real(dp), parameter :: B2_denom_0  =  1.7563773044362474D+00
         real(dp), parameter :: B2_0  =  1.0173166567483392D-14
         real(dp), parameter :: E1_0  =  -2.2308014220480969D+00
         real(dp), parameter :: F1_0  =  1.5176626709750911D-04
         real(dp), parameter :: E2_0  =  -2.2350904778008243D+01
         real(dp), parameter :: F2_0  =  2.7741209323605414D-13
         real(dp), parameter :: T_0  =  7.9432823472428218D+07
         real(dp), parameter :: RHO_0  =  3.1622776917911558D+09
         real(dp), parameter :: r_0  =  2.2348420508311778D+20
         real(dp), parameter :: G1_0  =  1.5177849505266735D-04
         real(dp), parameter :: G2_0  =  2.8758525980353755D-13
         real(dp), parameter :: U_0  =  1.0723431204522564D+00

         real(dp) :: tmp, &
               B1_denom, dB1_denom_dRho, dB1_denom_dT, &
               B2_denom, dB2_denom_dRho, dB2_denom_dT
         real(dp) :: A1, dA1dT, B1_numerator, dB1_numerator_dT
         real(dp) :: A2, dA2dT, B2_numerator, dB2_numerator_dT

         include 'formats.dek'
         
         R6=RHO*1d-6
         dR6dRho = 1d-6
         R6T=2.0*R6/UE
         dR6TdRho = 2.0*dR6dRho/UE

         R6T16=pow_cr(R6T,1d0/6d0)
         dR6T16dRho = (1d0/6d0)*dR6TdRho*R6T16/R6T
         R6T13=R6T16*R6T16       
         dR6T13dRho = 2*R6T16*dR6T16dRho
         T6=T*1d-6
         dT6dT=1d-6
         dT62dT=2*T6*dT6dT
         T613=pow_cr(T6,1d0/3d0)
         dT613dT=(1d0/3d0)*dT6dT*T613/T6
         T623=T613*T613
         dT623dT=2*T613*dT613dT
         T653=T623*T6
         dT653dT = dT623dT*T6 + T623*dT6dT
         
         T62=T6*T6
         T612=sqrt(T6)
         dT612dT=0.5*dT6dT/T612
         T632=T6*T612
         dT632dT=1.5*T612*dT6dT        
         
         U=1.35D0*R6T13/T623
         dUdT = -U * dT623dT / T623
         dUdRho = U * dR6T13dRho / R6T13
         
         U32 = U*sqrt(U)
         U52 = U*U32

         if (U < 1) then ! strong screening regime, eqn 4.8a in F&L
         
            A1 = (1-4.222D-2*T623)**2 + 2.643D-5*T653
            dA1dT = -2*4.222d-2*dT623dT*(1-4.222D-2*T623) + 2.643D-5*dT653dT
            
            B1_denom=A1*T623
            dB1_denom_dT = dA1dT*T623 + A1*dT623dT
            
            B1_numerator = 16.16D0*exp_cr(-134.92/T613)
            dB1_numerator_dT = B1_numerator*134.92*dT613dT/T613**2
            
            B1=B1_numerator/B1_denom
            dB1dT = dB1_numerator_dT/B1_denom - B1*dB1_denom_dT/B1_denom
            
            A2 = (1-2.807D-2*T623)**2 + 2.704D-6*T653
            dA2dT = -2*2.807D-2*dT623dT*(1-2.807D-2*T623) + 2.704D-6*dT653dT
            
            B2_denom=A2*T623
            dB2_denom_dT = dA2dT*T623 + A2*dT623dT
            
            B2_numerator = 244.6D0*pow5(1.0+3.528D-3*T623) * exp_cr(-235.72D0/T613)
            dB2_numerator_dT = B2_numerator* &
                  (5*3.528D-3*dT623dT/(1.0+3.528D-3*T623) + 235.72D0*dT613dT/T623)

            B2=B2_numerator/B2_denom
            dB2dT = dB2_numerator_dT/B2_denom - B2*dB2_denom_dT/B2_denom
            
            if (5.458D3 > R6T) then
            
               E1 = -1065.1D0/T6
               dE1dT = -E1 * dT6dT / T6
               
               F1 = exp_cr(E1)/T632
               dF1dT = F1 * (dE1dT - dT632dT/T632)
               
               B1=B1+F1
               dB1dT = dB1dT + dF1dT
               
            endif
            
            if (1.836D4 > R6T) then
            
               E2 = -3336.4D0/T6
               dE2dT = -E2 * dT6dT / T6
               
               F2 = exp_cr(E2)/T632
               dF2dT = F2 * (dE2dT - dT632dT/T632)
            
               B2=B2+F2
               dB2dT = dB2dT + dF2dT
               
            endif
            
            G1=B1*exp_cr(60.492D0*R6T13/T6)
            dG1dT = G1*(dB1dT/B1 - 60.492D0*R6T13*dT6dT/T6**2)
            dG1dRho=0
            
            G2=B2*exp_cr(106.35D0*R6T13/T6)
            dG2dT = G2*(dB2dT/B2 - 106.35D0*R6T13*dT6dT/T6**2)
            dG2dRho=0            

         else ! pycnonuclear regime, eqn 4.8b in F&L
         
            AF=1.0/U32 + 1.0
            dAFdT = -1.5 * dUdT/U52
            dAFdRho = -1.5 * dUdRho/U52
         
            B1_denom=T612*((1.0-5.680D-2*R6T13)**2+8.815D-7*T62)
            dB1_denom_dT = B1_denom*dT612dT/T612 + T612*8.815D-7*dT62dT
            dB1_denom_dRho = -2*5.680D-2*(1.0-5.680D-2*R6T13)*T612*dR6T13dRho
            
            B1=1.178D0*AF*exp_cr(-77.554/R6T16)/B1_denom
            dB1dT = B1 * (dAFdT/AF - dB1_denom_dT/B1_denom)
            dB1dRho = B1 * (dAFdRho/AF + 77.554*dR6T16dRho/(R6T16*R6T16) - dB1_denom_dRho/B1_denom)
         
            B2_denom=T612*((1.0-3.791D-2*R6T13)**2+5.162D-8*T62)
            dB2_denom_dT = B2_denom*dT612dT/T612 + T612*5.162D-8*dT62dT
            
            tmp = pow_cr(Rho/UE,1d0/3d0)
            dB2_denom_dRho = T612*(-0.000252733 + 9.58112d-8*tmp)*tmp/Rho
            
            B2=13.48D0*AF*pow5(1.0+5.070D-3*R6T13)*exp_cr(-135.08D0/R6T16)/B2_denom
            dB2dT = B2 * (dAFdT/AF - dB2_denom_dT/B2_denom)
            dB2dRho = B2 * (dAFdRho/AF + 135.08D0*dR6T16dRho/(R6T16*R6T16) - dB2_denom_dRho/B2_denom)            
            
            if (5.458D3 > R6T) then
            
               E1 = (60.492*R6T13-1065.1D0)/T6
               dE1dT = -E1 * dT6dT / T6
               dE1dRho = 60.492*dR6T13dRho/T6
               
               F1 = exp_cr(E1)/T632
               dF1dT = F1 * (dE1dT - dT632dT/T632)
               dF1dRho = F1 * dE1dRho
               
               !write(*,1) 'E1', E1
               !write(*,1) 'F1', F1

               G1=B1+F1
               dG1dT = dB1dT + dF1dT
               dG1dRho = dB1dRho + dF1dRho
               
            else
            
               G1=B1; dG1dRho = dB1dRho; dG1dT = dB1dT
               
            endif
            
            if (1.836D4 > R6T) then
            
               E2 = (106.35D0*R6T13-3336.4D0)/T6
               dE2dT = -E2 * dT6dT / T6
               dE2dRho = 106.35D0*dR6T13dRho/T6
               
               F2 = exp_cr(E2)/T632
               dF2dT = F2 * (dE2dT - dT632dT/T632)
               dF2dRho = F2 * dE2dRho
               
               !write(*,1) 'E2', E2
               !write(*,1) 'F2', F2
            
               G2=B2+F2
               dG2dT = dB2dT + dF2dT
               dG2dRho = dB2dRho + dF2dRho
               
            else
            
               G2=B2; dG2dRho = dB2dRho; dG2dT = dB2dT
               
            endif

         endif
      
         r=5.120D29*G1*G2*Y*Y*Y*R6*R6 ! ergs/g/sec, eqn 4.7 in F&L

         if (r < 1d-99 .or. G1 < 1d-99 .or. G2 < 1d-99) then
            drdT = 0
            drdRho = 0
         else
            drdT = r * (dG1dT/G1 + dG2dT/G2)
            drdRho = r * (dG1dRho/G1 + dG2dRho/G2 + 2*dR6dRho/R6)

            return
         
            write(*,1) 'T', T
            write(*,1) 'RHO', RHO
            write(*,1) 'r', r
            write(*,1) 'G1', G1
            write(*,1) 'G2', G2
            write(*,1) 'U', U
            write(*,*)
            
            write(*,1) 'abs(Rho_0 - Rho)', abs(Rho_0 - Rho)
            
            if (.true. .and. abs(Rho_0 - Rho) > 1d-2) then
               write(*,*)
               write(*,1) 'analytic drdRho', drdRho
               write(*,1) 'numeric drdRho', (r_0 - r) / (Rho_0 - Rho)
               write(*,*)
               write(*,1) 'analytic dG1dRho', dG1dRho
               write(*,1) 'numeric dG1dRho', (G1_0 - G1) / (Rho_0 - Rho)
               write(*,*)
               write(*,1) 'analytic dG2dRho', dG2dRho
               write(*,1) 'numeric dG2dRho', (G2_0 - G2) / (Rho_0 - Rho)
               write(*,*)
               write(*,1) 'analytic dUdRho', dUdRho
               write(*,1) 'numeric dUdRho', (U_0 - U) / (Rho_0 - Rho)
               write(*,*)
               write(*,1) 'analytic AF', dAFdRho 
               write(*,1) 'numeric AF', (AF_0 - AF) / (Rho_0 - Rho)
               write(*,*)
               write(*,1) 'analytic B1_denom', dB1_denom_dRho
               write(*,1) 'numeric B1_denom', (B1_denom_0 - B1_denom) / (Rho_0 - Rho)
               write(*,*)
               write(*,1) 'analytic B1', dB1dRho
               write(*,1) 'numeric B1', (B1_0 - B1) / (Rho_0 - Rho)
               write(*,*)
               write(*,1) 'analytic B2_denom', dB2_denom_dRho 
               write(*,1) 'numeric B2_denom', (B2_denom_0 - B2_denom) / (Rho_0 - Rho)
               write(*,*)
               write(*,1) 'analytic B2', dB2dRho 
               write(*,1) 'numeric B2', (B2_0 - B2) / (Rho_0 - Rho)
               write(*,*)
               write(*,1) 'analytic E1', dE1dRho 
               write(*,1) 'numeric E1', (E1_0 - E1) / (Rho_0 - Rho)
               write(*,*)
               write(*,1) 'analytic F1', dF1dRho 
               write(*,1) 'numeric F1', (F1_0 - F1) / (Rho_0 - Rho)
               write(*,*)
               write(*,1) 'analytic E2', dE2dRho 
               write(*,1) 'numeric E2', (E2_0 - E2) / (Rho_0 - Rho)
               write(*,*)
               write(*,1) 'analytic F2', dF2dRho 
               write(*,1) 'numeric F2', (F2_0 - F2) / (Rho_0 - Rho)
               write(*,*)
               stop 'FL_epsnuc_3alf' 
            end if
                        
         end if

      end subroutine FL_epsnuc_3alf

      real(dp) function tempRateAdjust(A1, Z1, X1, A2, Z2, X2, rho, Temp, inlowFlag)

         implicit none

         real(dp), intent(in)      :: A1, Z1, A2, Z2
         real(dp), intent(in)      :: X1, X2
         real(dp), intent(in)      :: rho
         real(dp), intent(in)      :: Temp   
         logical, intent(in), optional   :: inlowFlag
               
         real(dp)  :: beta_3halves_rt
         real(dp)  :: beta_factor
         real(dp)  :: inv_len_neg_sqrt
         logical   :: lowFlag
         real(dp)  :: param1, param2, param3, param4
         real(dp)  :: factor
       
         !print *,"tempRateAdjust: rho = ",rho," Temp=",Temp
         if (present(inlowFlag)) then
            lowFlag = inlowFlag
         else
            lowFlag = .true.
         end if

         if (lowFlag) then
            param1 = 0.0430_dp
            param2 = 1.2624_dp
            param3 = 1.2231_dp
            param4 = 0.6310_dp
         else
            param1 = 0.0485_dp
            param2 = 2.9314_dp
            param3 = 1.4331_dp
            param4 = 1.4654_dp
         end if

         beta_3halves_rt = beta_excitation2comp(A1,Z1,X1,A2,Z2,X2,rho,Temp)**(3.0_dp/2.0_dp)
         beta_factor = exp(-8.7833_dp*beta_3halves_rt)
         inv_len_neg_sqrt = inv_len_param2comp (rho, A1, Z1, X1, A2, Z2, X2)**(-1.0_dp/2.0_dp)

         factor = 1.0_dp - param4*beta_factor
         factor = inv_len_neg_sqrt*param3*beta_factor*factor 
         factor = exp(-7.272_dp*beta_3halves_rt + factor)
         factor = ((1.0_dp + param2*beta_factor)**(-1.0_dp/2.0_dp))*factor
         factor = 1.0_dp + param1*inv_len_neg_sqrt*factor

         tempRateAdjust = factor
                    
      end function tempRateAdjust

      real(dp) function beta_excitation2comp(A1,Z1,X1,A2,Z2,X2,rho,Temp)

         implicit none

         real(dp), intent(in)  :: A1   ! Atomic Weight species 1
         real(dp), intent(in)  :: Z1   ! Atomic Number species 1
         real(dp), intent(in)  :: X1   ! Fraction of species1
         real(dp), intent(in)  :: A2   ! Atomic Weight species 2
         real(dp), intent(in)  :: Z2   ! Atomic Number species 2
         real(dp), intent(in)  :: X2   ! Fraction of species2
         real(dp), intent(in)  :: rho  ! mass density in g/cm^3
         real(dp), intent(in)  :: Temp ! Temperature in Kelvin

         real(dp)  :: part1
         real(dp)  :: part2
         real(dp)  :: part3
         real(dp)  :: mu_e
         real(dp)  :: fivethirds, onethird


         fivethirds = (5.0_dp/3.0_dp)
         onethird = (1.0_dp/3.0_dp)

         mu_e = mean_wt_electron2comp(A1,Z1,X1,A2,Z2,X2)

         part1 = (Z1 + Z2)**(fivethirds) - Z1**(fivethirds) - Z2**(fivethirds)

         part2 = (Z1**2.0_dp)*(Z2**2.0_dp)*A1*A2
         part2 = (part2/(A1+A2))**onethird

         part1 = part1/part2


         part3 = (42579000_dp/Temp)*(((rho/mu_e)/16023000000_dp)**onethird)

         part1 = part1*part3

         beta_excitation2comp = part1

      end function beta_excitation2comp

      real(dp) function mean_wt_electron2comp(A1,Z1,X1,A2,Z2,X2)

         use const_def, only: me, amu

         implicit none

         real(dp), intent(in) :: A1 ! Atomic Weight species 1
         real(dp), intent(in) :: Z1 ! Atomic Number species 1
         real(dp), intent(in) :: X1 ! Fraction of species 1
         real(dp), intent(in) :: A2 ! Atomic Weight species 2
         real(dp), intent(in) :: Z2 ! Atomic Number species 2
         real(dp), intent(in) :: X2 ! Fraction of species 2

         real(dp) :: part1
         real(dp) :: part2
         real(dp) :: mu_e

         part1 = (X1*Z1)/A1
         part1 = part1/(1.0_dp + (Z1*me)/(A1*amu))

         part2 = (X2*Z2)/A2
         part2 = part2/(1.0_dp + (Z2*me)/(A2*amu))

         mu_e = 1.0_dp/(part1+part2)

         mean_wt_electron2comp = mu_e

      end function mean_wt_electron2comp

      real(dp) function inv_len_param2comp (rho, A1, Z1, X1, A2, Z2, X2)

         implicit none

         real(dp), intent(in)    :: rho  ! mass density 
         real(dp), intent(in)    :: A1   ! Atomic Weight
         real(dp), intent(in)    :: Z1   ! Atomic Number
         real(dp), intent(in)    :: X1   ! Fraction of species 1
         real(dp), intent(in)    :: A2   ! Atomic Weight
         real(dp), intent(in)    :: Z2   ! Atomic Number
         real(dp), intent(in)    :: X2   ! Fraction of species 2

         real(dp) :: mu_e

         mu_e = mean_wt_electron2comp(A1, Z1, X1, A2, Z2, X2)

         inv_len_param2comp = ((A1+A2)/(2.0_dp*A1*A2*Z1*Z2))*((rho/((Z1*mu_e)*1.3574d11))**(1.0_dp/3.0_dp))


      end function inv_len_param2comp

      integer function locate(xx,x)

         implicit none

         real(dp), dimension(:), intent(in) :: xx
         real(dp), intent(in) :: x

         integer :: n,jl,jm,ju
         logical :: ascnd

         n=size(xx)

         ascnd = (xx(n) >= xx(1))
         jl=0
         ju=n+1
         do
            if (ju-jl <= 1) exit
            jm=(ju+jl)/2
            if (ascnd .eqv. (x >= xx(jm))) then
               jl=jm
            else
               ju=jm
            end if
         end do
         if (x == xx(1)) then
            locate=1
         else if (x == xx(n)) then
            locate=n-1
         else
            locate=jl
         end if

      end function locate


      real(dp) function M3Y_Rate(rho, pycnoType)
      
         implicit none
      
         real(dp), intent(in) :: rho
         integer,  intent(in) :: pycnoType 
         
         integer, parameter   :: NsplinePts = 101
         integer              :: khi,klo,n
         real(dp)             :: a,b
         real(dp)             :: h
         real(dp)             :: ratehi, ratelo, derivs2hi, derivs2lo


         real(dp),  dimension(NsplinePts) :: densities = (/ &
  1.00000E+09,  1.99000E+09,  2.98000E+09,  3.97000E+09,  4.96000E+09, &
  5.95000E+09,  6.94000E+09,  7.93000E+09,  8.92000E+09,  9.91000E+09, &
  1.09000E+10,  1.18900E+10,  1.28800E+10,  1.38700E+10,  1.48600E+10, &
  1.58500E+10,  1.68400E+10,  1.78300E+10,  1.88200E+10,  1.98100E+10, &
  2.08000E+10,  2.17900E+10,  2.27800E+10,  2.37700E+10,  2.47600E+10, &
  2.57500E+10,  2.67400E+10,  2.77300E+10,  2.87200E+10,  2.97100E+10, &
  3.07000E+10,  3.16900E+10,  3.26800E+10,  3.36700E+10,  3.46600E+10, &
  3.56500E+10,  3.66400E+10,  3.76300E+10,  3.86200E+10,  3.96100E+10, &
  4.06000E+10,  4.15900E+10,  4.25800E+10,  4.35700E+10,  4.45600E+10, &
  4.55500E+10,  4.65400E+10,  4.75300E+10,  4.85200E+10,  4.95100E+10, &
  5.05000E+10,  5.14900E+10,  5.24800E+10,  5.34700E+10,  5.44600E+10, &
  5.54500E+10,  5.64400E+10,  5.74300E+10,  5.84200E+10,  5.94100E+10, &
  6.04000E+10,  6.13900E+10,  6.23800E+10,  6.33700E+10,  6.43600E+10, &
  6.53500E+10,  6.63400E+10,  6.73300E+10,  6.83200E+10,  6.93100E+10, &
  7.03000E+10,  7.12900E+10,  7.22800E+10,  7.32700E+10,  7.42600E+10, &
  7.52500E+10,  7.62400E+10,  7.72300E+10,  7.82200E+10,  7.92100E+10, &
  8.02000E+10,  8.11900E+10,  8.21800E+10,  8.31700E+10,  8.41600E+10, &
  8.51500E+10,  8.61400E+10,  8.71300E+10,  8.81200E+10,  8.91100E+10, &
  9.01000E+10,  9.10900E+10,  9.20800E+10,  9.30700E+10,  9.40600E+10, &
  9.50500E+10,  9.60400E+10,  9.70300E+10,  9.80200E+10,  9.90100E+10, &
  1.00000E+11 &
         /)
         
         real(dp),  dimension(NsplinePts) :: ratesM3Y_SPVH = (/ &
  9.99826E-16,  2.04871E-06,  2.09537E-01,  4.90925E+02,  1.59846E+05, &
  1.56040E+07,  6.79538E+08,  1.66248E+10,  2.63966E+11,  3.00151E+12, &
  2.61758E+13,  1.83821E+14,  1.07955E+15,  5.45207E+15,  2.42134E+16, &
  9.63289E+16,  3.47632E+17,  1.15514E+18,  3.55586E+18,  1.02593E+19, &
  2.78768E+19,  7.17758E+19,  1.76023E+20,  4.13407E+20,  9.32788E+20, &
  2.02836E+21,  4.26711E+21,  8.69356E+21,  1.72190E+22,  3.32247E+22, &
  6.24789E+22,  1.14879E+23,  2.06704E+23,  3.64705E+23,  6.30850E+23, &
  1.07312E+24,  1.79478E+24,  2.95211E+24,  4.78783E+24,  7.66573E+24, &
  1.20862E+25,  1.88316E+25,  2.90043E+25,  4.41730E+25,  6.65689E+25, &
  9.93614E+25,  1.46903E+26,  2.15325E+26,  3.13617E+26,  4.49791E+26, &
  6.39386E+26,  9.02124E+26,  1.26346E+27,  1.75665E+27,  2.42516E+27, &
  3.32593E+27,  4.53318E+27,  6.14307E+27,  8.26243E+27,  1.10592E+28, &
  1.47387E+28,  1.94844E+28,  2.56797E+28,  3.36380E+28,  4.38488E+28, &
  5.70086E+28,  7.36004E+28,  9.46418E+28,  1.21220E+29,  1.54660E+29, &
  1.96567E+29,  2.48868E+29,  3.13865E+29,  3.94333E+29,  4.93656E+29, &
  6.15923E+29,  7.66035E+29,  9.49854E+29,  1.17439E+30,  1.44801E+30, &
  1.77544E+30,  2.17484E+30,  2.65836E+30,  3.23408E+30,  3.92867E+30, &
  4.76516E+30,  5.74894E+30,  6.93585E+30,  8.33138E+30,  1.00014E+31, &
  1.19596E+31,  1.42930E+31,  1.70125E+31,  2.02533E+31,  2.39967E+31, &
  2.84161E+31,  3.35853E+31,  3.95714E+31,  4.66086E+31,  5.47470E+31, &
  6.41434E+31 &
         /)
         
         real(dp),  dimension(NsplinePts) :: ratesM3Y_bcc_static = (/ &
  3.91504E-14,  8.13511E-05,  8.38290E+00,  1.97392E+04,  6.45113E+06, &
  6.31617E+08,  2.75730E+10,  6.75958E+11,  1.07518E+13,  1.22447E+14, &
  1.06933E+15,  7.51877E+15,  4.42065E+16,  2.23487E+17,  9.93482E+17, &
  3.95588E+18,  1.42877E+19,  4.75124E+19,  1.46362E+20,  4.22564E+20, &
  1.14893E+21,  2.96000E+21,  7.26322E+21,  1.70676E+22,  3.85303E+22, &
  8.38258E+22,  1.76429E+23,  3.59609E+23,  7.12573E+23,  1.37550E+24, &
  2.58767E+24,  4.75972E+24,  8.56745E+24,  1.51217E+25,  2.61658E+25, &
  4.45247E+25,  7.44914E+25,  1.22564E+26,  1.98839E+26,  3.18452E+26, &
  5.02231E+26,  7.82751E+26,  1.20591E+27,  1.83707E+27,  2.76918E+27, &
  4.13435E+27,  6.11403E+27,  8.96382E+27,  1.30587E+28,  1.87331E+28, &
  2.66353E+28,  3.75885E+28,  5.26554E+28,  7.32246E+28,  1.01111E+29, &
  1.38694E+29,  1.89075E+29,  2.56270E+29,  3.44748E+29,  4.61527E+29, &
  6.15193E+29,  8.13421E+29,  1.07224E+30,  1.40478E+30,  1.83150E+30, &
  2.38155E+30,  3.07518E+30,  3.95496E+30,  5.06642E+30,  6.46504E+30, &
  8.21805E+30,  1.04062E+31,  1.31259E+31,  1.64934E+31,  2.06506E+31, &
  2.57689E+31,  3.20536E+31,  3.97506E+31,  4.91538E+31,  6.06141E+31, &
  7.43295E+31,  9.10622E+31,  1.11322E+32,  1.35447E+32,  1.64558E+32, &
  1.99619E+32,  2.40859E+32,  2.90621E+32,  3.49135E+32,  4.19167E+32, &
  5.01293E+32,  5.99167E+32,  7.13247E+32,  8.49205E+32,  1.00627E+33, &
  1.19172E+33,  1.40865E+33,  1.65990E+33,  1.95529E+33,  2.29693E+33, &
  2.69143E+33 &
         /)

         real(dp),  dimension(NsplinePts) :: ratesM3Y_bcc_ws = (/ &
  4.11442E-10,  3.30472E-01,  2.04686E+04,  3.42587E+07,  8.68553E+09, &
  6.95762E+11,  2.57524E+13,  5.49064E+14,  7.74111E+15,  7.92999E+16, &
  6.30244E+17,  4.07138E+18,  2.21662E+19,  1.04457E+20,  4.35275E+20, &
  1.63256E+21,  5.57749E+21,  1.76090E+22,  5.16684E+22,  1.42500E+23, &
  3.71082E+23,  9.17766E+23,  2.16647E+24,  4.90693E+24,  1.06958E+25, &
  2.25037E+25,  4.58724E+25,  9.06781E+25,  1.74476E+26,  3.27422E+26, &
  5.99459E+26,  1.07417E+27,  1.88535E+27,  3.24766E+27,  5.48896E+27, &
  9.13012E+27,  1.49423E+28,  2.40661E+28,  3.82434E+28,  6.00311E+28, &
  9.28461E+28,  1.41987E+29,  2.14750E+29,  3.21327E+29,  4.75974E+29, &
  6.98621E+29,  1.01613E+30,  1.46582E+30,  2.10195E+30,  2.96912E+30, &
  4.15841E+30,  5.78261E+30,  7.98458E+30,  1.09482E+31,  1.49105E+31, &
  2.01783E+31,  2.71466E+31,  3.63204E+31,  4.82434E+31,  6.37858E+31, &
  8.39911E+31,  1.09732E+32,  1.42956E+32,  1.85142E+32,  2.38660E+32, &
  3.06901E+32,  3.91974E+32,  4.98724E+32,  6.32164E+32,  7.98341E+32, &
  1.00449E+33,  1.25923E+33,  1.57270E+33,  1.95704E+33,  2.42696E+33, &
  3.00004E+33,  3.69721E+33,  4.54326E+33,  5.56757E+33,  6.80495E+33, &
  8.27201E+33,  1.00471E+34,  1.21783E+34,  1.46939E+34,  1.77048E+34, &
  2.13025E+34,  2.54975E+34,  3.05218E+34,  3.63809E+34,  4.33418E+34, &
  5.14395E+34,  6.10208E+34,  7.21003E+34,  8.52153E+34,  1.00246E+35, &
  1.17872E+35,  1.38345E+35,  1.61883E+35,  1.89376E+35,  2.20950E+35, &
  2.57155E+35 &
         /)

         real(dp),  dimension(NsplinePts) :: ratesM3Y_bcc_relaxed = (/ &
  3.68466E-10,  2.98251E-01,  1.85495E+04,  3.11329E+07,  7.90936E+09, &
  6.34621E+11,  2.35209E+13,  5.02057E+14,  7.08531E+15,  7.26444E+16, &
  5.77791E+17,  3.73511E+18,  2.03481E+19,  9.59440E+19,  4.00012E+20, &
  1.50103E+21,  5.13045E+21,  1.62045E+22,  4.75661E+22,  1.31235E+23, &
  3.41866E+23,  8.45789E+23,  1.99719E+24,  4.52487E+24,  9.86579E+24, &
  2.07631E+25,  4.23353E+25,  8.37071E+25,  1.61102E+26,  3.02392E+26, &
  5.53757E+26,  9.92489E+26,  1.74234E+27,  3.00190E+27,  5.07457E+27, &
  8.44241E+27,  1.38192E+28,  2.22612E+28,  3.53812E+28,  5.55474E+28, &
  8.59250E+28,  1.31423E+29,  1.98802E+29,  2.97508E+29,  4.40754E+29, &
  6.47016E+29,  9.41201E+29,  1.35791E+30,  1.94745E+30,  2.75123E+30, &
  3.85372E+30,  5.35955E+30,  7.40129E+30,  1.01496E+31,  1.38244E+31, &
  1.87106E+31,  2.51747E+31,  3.36856E+31,  4.47483E+31,  5.91707E+31, &
  7.79219E+31,  1.01813E+32,  1.32652E+32,  1.71812E+32,  2.21498E+32, &
  2.84858E+32,  3.63853E+32,  4.62985E+32,  5.86913E+32,  7.41257E+32, &
  9.32748E+32,  1.16938E+33,  1.46061E+33,  1.81770E+33,  2.25433E+33, &
  2.78687E+33,  3.43475E+33,  4.22105E+33,  5.17311E+33,  6.32328E+33, &
  7.68704E+33,  9.33725E+33,  1.13187E+34,  1.36576E+34,  1.64573E+34, &
  1.98028E+34,  2.37040E+34,  2.83768E+34,  3.38262E+34,  4.03009E+34, &
  4.78333E+34,  5.67465E+34,  6.70540E+34,  7.92557E+34,  9.32409E+34, &
  1.09641E+35,  1.28693E+35,  1.50597E+35,  1.76183E+35,  2.05569E+35, &
  2.39266E+35 &
         /)

         real(dp),  dimension(NsplinePts) :: ratesM3Y_fcc_static = (/ &
  1.23164E-15,  3.94601E-06,  5.12742E-01,  1.41048E+03,  5.17503E+05, &
  5.55179E+07,  2.61284E+09,  6.82602E+10,  1.14707E+12,  1.37092E+13, &
  1.24974E+14,  9.13312E+14,  5.56121E+15,  2.90295E+16,  1.32904E+17, &
  5.43820E+17,  2.01454E+18,  6.85950E+18,  2.16043E+19,  6.36880E+19, &
  1.76604E+20,  4.63527E+20,  1.15764E+21,  2.76630E+21,  6.34551E+21, &
  1.40172E+22,  2.99354E+22,  6.18739E+22,  1.24257E+23,  2.42963E+23, &
  4.62762E+23,  8.61401E+23,  1.56843E+24,  2.79917E+24,  4.89572E+24, &
  8.41750E+24,  1.42248E+25,  2.36334E+25,  3.87041E+25,  6.25568E+25, &
  9.95389E+25,  1.56482E+26,  2.43111E+26,  3.73392E+26,  5.67348E+26, &
  8.53638E+26,  1.27198E+27,  1.87866E+27,  2.75666E+27,  3.98242E+27, &
  5.70136E+27,  8.10014E+27,  1.14217E+28,  1.59858E+28,  2.22129E+28, &
  3.06576E+28,  4.20465E+28,  5.73270E+28,  7.75669E+28,  1.04432E+29, &
  1.39980E+29,  1.86098E+29,  2.46630E+29,  3.24821E+29,  4.25682E+29, &
  5.56342E+29,  7.21966E+29,  9.33071E+29,  1.20106E+30,  1.53989E+30, &
  1.96657E+30,  2.50162E+30,  3.16967E+30,  4.00057E+30,  5.03084E+30, &
  6.30477E+30,  7.87568E+30,  9.80766E+30,  1.21776E+31,  1.50777E+31, &
  1.85633E+31,  2.28318E+31,  2.80197E+31,  3.42227E+31,  4.17348E+31, &
  5.08155E+31,  6.15391E+31,  7.45221E+31,  8.98468E+31,  1.08250E+32, &
  1.29910E+32,  1.55808E+32,  1.86103E+32,  2.22321E+32,  2.64315E+32, &
  3.14049E+32,  3.72417E+32,  4.40242E+32,  5.20223E+32,  6.13027E+32, &
  7.20528E+32 &
         /)

         real(dp),  dimension(NsplinePts) :: ratesM3Y_fcc_ws = (/ &
  1.19522E-10,  1.17706E-01,  8.13130E+03,  1.46431E+07,  3.92021E+09, &
  3.27841E+11,  1.25716E+13,  2.76183E+14,  3.99586E+15,  4.18739E+16, &
  3.39591E+17,  2.23399E+18,  1.23649E+19,  5.91536E+19,  2.49937E+20, &
  9.49525E+20,  3.28287E+21,  1.04806E+22,  3.10748E+22,  8.65485E+22, &
  2.27475E+23,  5.67544E+23,  1.35091E+24,  3.08398E+24,  6.77297E+24, &
  1.43528E+25,  2.94588E+25,  5.86166E+25,  1.13499E+26,  2.14285E+26, &
  3.94615E+26,  7.11089E+26,  1.25485E+27,  2.17288E+27,  3.69101E+27, &
  6.16951E+27,  1.01448E+28,  1.64142E+28,  2.61997E+28,  4.13035E+28, &
  6.41492E+28,  9.85013E+28,  1.49570E+29,  2.24662E+29,  3.34036E+29, &
  4.92082E+29,  7.18279E+29,  1.03976E+30,  1.49605E+30,  2.12026E+30, &
  2.97915E+30,  4.15587E+30,  5.75616E+30,  7.91656E+30,  1.08136E+31, &
  1.46765E+31,  1.98008E+31,  2.65660E+31,  3.53831E+31,  4.69075E+31, &
  6.19284E+31,  8.11158E+31,  1.05943E+32,  1.37546E+32,  1.77737E+32, &
  2.29105E+32,  2.93300E+32,  3.74038E+32,  4.75192E+32,  6.01444E+32, &
  7.58414E+32,  9.52796E+32,  1.19251E+33,  1.48704E+33,  1.84789E+33, &
  2.28887E+33,  2.82639E+33,  3.47999E+33,  4.27284E+33,  5.23242E+33, &
  6.37242E+33,  7.75420E+33,  9.41621E+33,  1.13817E+34,  1.37382E+34, &
  1.65589E+34,  1.98539E+34,  2.38068E+34,  2.84245E+34,  3.39193E+34, &
  4.03225E+34,  4.79106E+34,  5.67001E+34,  6.71194E+34,  7.90815E+34, &
  9.31292E+34,  1.09471E+35,  1.28289E+35,  1.50299E+35,  1.75615E+35, &
  2.04688E+35 &
         /)

         real(dp),  dimension(NsplinePts) :: ratesM3Y_fcc_relaxed = (/ &
  1.66294E-10,  1.56816E-01,  1.05844E+04,  1.87661E+07,  4.96612E+09, &
  4.11523E+11,  1.56621E+13,  3.41894E+14,  4.91943E+15,  5.13037E+16, &
  4.14280E+17,  2.71481E+18,  1.49736E+19,  7.14043E+19,  3.00810E+20, &
  1.13968E+21,  3.93032E+21,  1.25179E+22,  3.70330E+22,  1.02928E+23, &
  2.69994E+23,  6.72372E+23,  1.59760E+24,  3.64103E+24,  7.98357E+24, &
  1.68924E+25,  3.46207E+25,  6.87914E+25,  1.33022E+26,  2.50821E+26, &
  4.61325E+26,  8.30308E+26,  1.46355E+27,  2.53145E+27,  4.29549E+27, &
  7.17244E+27,  1.17821E+28,  1.90448E+28,  3.03699E+28,  4.78340E+28, &
  7.42256E+28,  1.13875E+29,  1.72769E+29,  2.59296E+29,  3.85223E+29, &
  5.67048E+29,  8.27077E+29,  1.19637E+30,  1.72014E+30,  2.43614E+30, &
  3.42064E+30,  4.76852E+30,  6.60038E+30,  9.07180E+30,  1.23838E+31, &
  1.67972E+31,  2.26484E+31,  3.03685E+31,  4.04242E+31,  5.35600E+31, &
  7.06717E+31,  9.25176E+31,  1.20769E+32,  1.56712E+32,  2.02400E+32, &
  2.60763E+32,  3.33662E+32,  4.25302E+32,  5.40060E+32,  6.83222E+32, &
  8.61133E+32,  1.08135E+33,  1.35279E+33,  1.68616E+33,  2.09442E+33, &
  2.59310E+33,  3.20072E+33,  3.93923E+33,  4.83472E+33,  5.91809E+33, &
  7.20460E+33,  8.76338E+33,  1.06376E+34,  1.28530E+34,  1.55084E+34, &
  1.86855E+34,  2.23955E+34,  2.68446E+34,  3.20402E+34,  3.82205E+34, &
  4.54198E+34,  5.39485E+34,  6.38240E+34,  7.55272E+34,  8.89582E+34, &
  1.04726E+35,  1.23063E+35,  1.44171E+35,  1.68853E+35,  1.97233E+35, &
  2.29813E+35 &
         /)


         real(dp),  dimension(NsplinePts) :: derivs2M3Y_SPVH = (/ &
 8.74024E-12, -1.74805E-11,  6.11817E-11, -2.27243E-10,  8.48764E-10, &
 -3.07424E-09,  1.54181E-08,  3.49515E-08,  1.26134E-06,  1.01643E-05, &
  8.31918E-05,  4.80274E-04,  2.51414E-03,  1.07474E-02,  4.25818E-02, &
  1.45550E-01,  4.72176E-01,  1.37072E+00,  3.79836E+00,  9.77608E+00, &
  2.39116E+01,  5.54682E+01,  1.23654E+02,  2.64956E+02,  5.42862E+02, &
  1.09092E+03,  2.09183E+03,  3.93443E+03,  7.26390E+03,  1.28025E+04, &
  2.26315E+04,  3.83646E+04,  6.52659E+04,  1.05690E+05,  1.74008E+05, &
  2.76469E+05,  4.30536E+05,  6.68464E+05,  1.04855E+06,  1.51746E+06, &
  2.32466E+06,  3.41721E+06,  4.98749E+06,  7.21767E+06,  1.03854E+07, &
  1.48869E+07,  2.03613E+07,  3.14880E+07,  3.65476E+07,  5.42310E+07, &
  7.35609E+07,  9.92974E+07,  1.32857E+08,  1.76456E+08,  2.34548E+08, &
  3.07264E+08,  4.12638E+08,  5.06999E+08,  6.78321E+08,  9.26650E+08, &
  1.01920E+09,  1.52347E+09,  1.76096E+09,  2.22565E+09,  3.12544E+09, &
  3.32608E+09,  4.58093E+09,  5.58910E+09,  6.95909E+09,  8.58197E+09, &
  1.05440E+10,  1.28740E+10,  1.56802E+10,  1.91159E+10,  2.32862E+10, &
  2.81971E+10,  3.43840E+10,  4.06229E+10,  5.23900E+10,  5.03138E+10, &
  7.56905E+10,  8.75770E+10,  8.89759E+10,  1.20974E+11,  1.54793E+11, &
  1.28517E+11,  2.32867E+11,  1.83550E+11,  3.10003E+11,  2.56856E+11, &
  4.26868E+11,  3.32685E+11,  6.05775E+11,  4.35052E+11,  7.31785E+11, &
  7.75285E+11,  7.57903E+11,  1.19395E+12,  9.00912E+11,  1.94314E+12, &
 -9.71569E+11 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2M3Y_bcc_static = (/ &
  3.59410E-10, -7.18819E-10,  2.51587E-09, -9.34453E-09,  3.49015E-08, &
 -1.26474E-07,  6.32097E-07,  1.40245E-06,  5.14712E-05,  4.14810E-04, &
  3.40215E-03,  1.96623E-02,  1.03062E-01,  4.41018E-01,  1.74911E+00, &
  5.98405E+00,  1.94290E+01,  5.64465E+01,  1.56531E+02,  4.03150E+02, &
  9.86714E+02,  2.29030E+03,  5.10864E+03,  1.09524E+04,  2.24518E+04, &
  4.51404E+04,  8.65990E+04,  1.62953E+05,  3.00980E+05,  5.30700E+05, &
  9.38504E+05,  1.59157E+06,  2.70856E+06,  4.38783E+06,  7.22649E+06, &
  1.14856E+07,  1.78922E+07,  2.77883E+07,  4.36010E+07,  6.31212E+07, &
  9.67223E+07,  1.42221E+08,  2.07631E+08,  3.00554E+08,  4.32569E+08, &
  6.20219E+08,  8.48515E+08,  1.31239E+09,  1.52396E+09,  2.26153E+09, &
  3.06833E+09,  4.14273E+09,  5.54403E+09,  7.36496E+09,  9.79153E+09, &
  1.28298E+10,  1.72325E+10,  2.11788E+10,  2.83391E+10,  3.87190E+10, &
  4.26017E+10,  6.36775E+10,  7.36265E+10,  9.30692E+10,  1.30702E+11, &
  1.39144E+11,  1.91643E+11,  2.33864E+11,  2.91231E+11,  3.59203E+11, &
  4.41391E+11,  5.39012E+11,  6.56602E+11,  8.00581E+11,  9.75370E+11, &
  1.18123E+12,  1.44059E+12,  1.70224E+12,  2.19546E+12,  2.10932E+12, &
  3.17257E+12,  3.67150E+12,  3.73134E+12,  5.07280E+12,  6.49136E+12, &
  5.39253E+12,  9.76674E+12,  7.70321E+12,  1.30054E+13,  1.07817E+13, &
  1.79121E+13,  1.39689E+13,  2.54236E+13,  1.82723E+13,  3.07215E+13, &
  3.25559E+13,  3.18345E+13,  5.01403E+13,  3.78465E+13,  8.16352E+13, &
 -4.08176E+13 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2M3Y_bcc_ws = (/ &
  1.35200E-07, -2.70400E-07,  9.46400E-07, -3.51499E-06,  1.31663E-05, &
 -4.49971E-05,  3.16008E-04,  1.83120E-03,  3.31840E-02,  2.59475E-01, &
  1.86363E+00,  9.97926E+00,  4.79268E+01,  1.91309E+02,  7.08283E+02, &
  2.27992E+03,  6.99258E+03,  1.92547E+04,  5.08391E+04,  1.24939E+05, &
  2.92688E+05,  6.51674E+05,  1.39823E+06,  2.88772E+06,  5.71249E+06, &
  1.11101E+07,  2.06203E+07,  3.76424E+07,  6.75142E+07,  1.15609E+08, &
  1.99109E+08,  3.28706E+08,  5.45856E+08,  8.61751E+08,  1.38819E+09, &
  2.15521E+09,  3.28145E+09,  4.99270E+09,  7.68362E+09,  1.08632E+10, &
  1.63700E+10,  2.36024E+10,  3.38277E+10,  4.80948E+10,  6.80635E+10, &
  9.59401E+10,  1.28924E+11,  1.97537E+11,  2.22244E+11,  3.27932E+11, &
  4.37954E+11,  5.82676E+11,  7.68411E+11,  1.00635E+12,  1.31997E+12, &
  1.70600E+12,  2.26556E+12,  2.73404E+12,  3.62786E+12,  4.91229E+12, &
  5.26864E+12,  7.90076E+12,  8.94079E+12,  1.11926E+13,  1.56706E+13, &
  1.62504E+13,  2.23742E+13,  2.69495E+13,  3.32260E+13,  4.05492E+13, &
  4.93139E+13,  5.95970E+13,  7.18699E+13,  8.67990E+13,  1.04773E+14, &
  1.25711E+14,  1.51991E+14,  1.77740E+14,  2.28379E+14,  2.13097E+14, &
  3.25280E+14,  3.71474E+14,  3.69051E+14,  5.05030E+14,  6.43521E+14, &
  5.13093E+14,  9.60222E+14,  7.23584E+14,  1.25545E+15,  9.99891E+14, &
  1.70359E+15,  1.26914E+15,  2.39102E+15,  1.62743E+15,  2.82847E+15, &
  2.94446E+15,  2.82503E+15,  4.51572E+15,  3.32830E+15,  7.14919E+15, &
 -3.57460E+15 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2M3Y_bcc_relaxed = (/ &
  1.24404E-07, -2.48807E-07,  8.70825E-07, -3.23430E-06,  1.21144E-05, &
 -4.14350E-05,  2.89895E-04,  1.67126E-03,  3.03971E-02,  2.37780E-01, &
  1.70956E+00,  9.16010E+00,  4.40236E+01,  1.75828E+02,  6.51332E+02, &
  2.09764E+03,  6.43649E+03,  1.77312E+04,  4.68352E+04,  1.15143E+05, &
  2.69836E+05,  6.00996E+05,  1.28990E+06,  2.66480E+06,  5.27304E+06, &
  1.02582E+07,  1.90443E+07,  3.47740E+07,  6.23843E+07,  1.06851E+08, &
  1.84063E+08,  3.03935E+08,  5.04820E+08,  7.97135E+08,  1.28433E+09, &
  1.99434E+09,  3.03708E+09,  4.62167E+09,  7.11374E+09,  1.00594E+10, &
  1.51608E+10,  2.18625E+10,  3.13388E+10,  4.45627E+10,  6.30734E+10, &
  8.89185E+10,  1.19506E+11,  1.83120E+11,  2.06078E+11,  3.04092E+11, &
  4.06168E+11,  5.40450E+11,  7.12810E+11,  9.33638E+11,  1.22473E+12, &
  1.58309E+12,  2.10253E+12,  2.53768E+12,  3.36753E+12,  4.56011E+12, &
  4.89197E+12,  7.33565E+12,  8.30278E+12,  1.03948E+13,  1.45538E+13, &
  1.50957E+13,  2.07842E+13,  2.50370E+13,  3.08706E+13,  3.76780E+13, &
  4.58258E+13,  5.53863E+13,  6.67976E+13,  8.06792E+13,  9.73935E+13, &
  1.16865E+14,  1.41306E+14,  1.65259E+14,  2.12348E+14,  1.98188E+14, &
  3.02482E+14,  3.45477E+14,  3.43290E+14,  4.69747E+14,  5.98583E+14, &
  4.77432E+14,  8.93225E+14,  6.73362E+14,  1.16803E+15,  9.30586E+14, &
  1.58515E+15,  1.18138E+15,  2.22499E+15,  1.51514E+15,  2.63253E+15, &
  2.74089E+15,  2.63015E+15,  4.20365E+15,  3.09892E+15,  6.65669E+15, &
 -3.32835E+15 &
         /)
 
         real(dp),  dimension(NsplinePts) :: derivs2M3Y_fcc_static = (/ &
  5.14790E-11, -1.02958E-10,  3.60353E-10, -1.33844E-09,  4.99658E-09, &
 -1.83143E-08,  8.35795E-08,  7.02227E-08,  5.83793E-06,  4.68770E-05, &
  4.10892E-04,  2.45448E-03,  1.33987E-02,  5.91658E-02,  2.42171E-01, &
  8.51805E-01,  2.83854E+00,  8.45055E+00,  2.39646E+01,  6.30547E+01, &
  1.57438E+02,  3.72434E+02,  8.45579E+02,  1.84396E+03,  3.84187E+03, &
  7.84220E+03,  1.52729E+04,  2.91392E+04,  5.45473E+04,  9.74678E+04, &
  1.74463E+05,  2.99500E+05,  5.15437E+05,  8.44831E+05,  1.40558E+06, &
  2.25792E+06,  3.55412E+06,  5.57206E+06,  8.82066E+06,  1.29063E+07, &
  1.99305E+07,  2.95683E+07,  4.35317E+07,  6.35320E+07,  9.21447E+07, &
  1.33147E+08,  1.83641E+08,  2.85331E+08,  3.35908E+08,  5.00012E+08, &
  6.83260E+08,  9.28714E+08,  1.25118E+09,  1.67295E+09,  2.23790E+09, &
  2.95065E+09,  3.98407E+09,  4.93621E+09,  6.63162E+09,  9.09897E+09, &
  1.01228E+10,  1.51183E+10,  1.76451E+10,  2.24057E+10,  3.15193E+10, &
  3.39371E+10,  4.67713E+10,  5.74098E+10,  7.18211E+10,  8.90111E+10, &
  1.09894E+11,  1.34834E+11,  1.65005E+11,  2.02064E+11,  2.47226E+11, &
  3.00680E+11,  3.68158E+11,  4.37068E+11,  5.64787E+11,  5.49402E+11, &
  8.21674E+11,  9.56453E+11,  9.81548E+11,  1.33134E+12,  1.70715E+12, &
  1.44274E+12,  2.57904E+12,  2.07339E+12,  3.46269E+12,  2.91940E+12, &
  4.80160E+12,  3.81683E+12,  6.85092E+12,  5.03660E+12,  8.35740E+12, &
  8.92469E+12,  8.79798E+12,  1.37776E+13,  1.05039E+13,  2.27076E+13, &
 -1.13538E+13 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2M3Y_fcc_ws = (/ &
  8.02444E-08, -1.60489E-07,  5.61711E-07, -2.08627E-06,  7.80717E-06, &
 -2.71834E-05,  1.73897E-04,  8.70423E-04,  1.75018E-02,  1.38234E-01, &
  1.02025E+00,  5.55537E+00,  2.71809E+01,  1.10134E+02,  4.13793E+02, &
  1.34951E+03,  4.18977E+03,  1.16704E+04,  3.11393E+04,  7.72993E+04, &
  1.82793E+05,  4.10639E+05,  8.88427E+05,  1.84960E+06,  3.68695E+06, &
  7.22184E+06,  1.34993E+07,  2.48035E+07,  4.47677E+07,  7.71424E+07, &
  1.33613E+08,  2.21857E+08,  3.70357E+08,  5.87942E+08,  9.51598E+08, &
  1.48485E+09,  2.27191E+09,  3.47193E+09,  5.36535E+09,  7.62451E+09, &
  1.15304E+10,  1.66950E+10,  2.40226E+10,  3.42862E+10,  4.86969E+10, &
  6.88929E+10,  9.29359E+10,  1.42673E+11,  1.61640E+11,  2.38769E+11, &
  3.19961E+11,  4.27036E+11,  5.64936E+11,  7.42132E+11,  9.76230E+11, &
  1.26545E+12,  1.68458E+12,  2.04101E+12,  2.71299E+12,  3.68048E+12, &
  3.96977E+12,  5.94776E+12,  6.76260E+12,  8.48324E+12,  1.18840E+13, &
  1.23955E+13,  1.70655E+13,  2.06134E+13,  2.54686E+13,  3.11528E+13, &
  3.79707E+13,  4.59912E+13,  5.55825E+13,  6.72653E+13,  8.13559E+13, &
  9.78087E+13,  1.18475E+14,  1.38857E+14,  1.78552E+14,  1.67694E+14, &
  2.55092E+14,  2.92164E+14,  2.91726E+14,  3.98578E+14,  5.08323E+14, &
  4.09052E+14,  7.59778E+14,  5.78428E+14,  9.97303E+14,  8.01466E+14, &
  1.35762E+15,  1.02186E+15,  1.90996E+15,  1.31605E+15,  2.26986E+15, &
  2.37226E+15,  2.28568E+15,  3.64106E+15,  2.69800E+15,  5.80000E+15, &
 -2.90000E+15 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2M3Y_fcc_relaxed = (/ &
  9.59021E-08, -1.91804E-07,  6.71314E-07, -2.49334E-06,  9.33221E-06, &
 -3.23769E-05,  2.11048E-04,  1.09196E-03,  2.14468E-02,  1.69055E-01, &
  1.24046E+00,  6.73051E+00,  3.28003E+01,  1.32481E+02,  4.96198E+02, &
  1.61376E+03,  4.99719E+03,  1.38854E+04,  3.69667E+04,  9.15690E+04, &
  2.16105E+05,  4.84553E+05,  1.04649E+06,  2.17495E+06,  4.32845E+06, &
  8.46556E+06,  1.58002E+07,  2.89911E+07,  5.22558E+07,  8.99253E+07, &
  1.55566E+08,  2.57991E+08,  4.30197E+08,  6.82133E+08,  1.10294E+09, &
  1.71915E+09,  2.62766E+09,  4.01184E+09,  6.19423E+09,  8.79285E+09, &
  1.32872E+10,  1.92214E+10,  2.76346E+10,  3.94091E+10,  5.59300E+10, &
  7.90643E+10,  1.06570E+11,  1.63535E+11,  1.85002E+11,  2.73214E+11, &
  3.65855E+11,  4.87961E+11,  6.45102E+11,  8.46892E+11,  1.11335E+12, &
  1.44230E+12,  1.91900E+12,  2.32308E+12,  3.08677E+12,  4.18586E+12, &
  4.50950E+12,  6.75763E+12,  7.67573E+12,  9.62449E+12,  1.34810E+13, &
  1.40440E+13,  1.93353E+13,  2.33411E+13,  2.88256E+13,  3.52420E+13, &
  4.29346E+13,  5.19788E+13,  6.27899E+13,  7.59546E+13,  9.18265E+13, &
  1.10350E+14,  1.33614E+14,  1.56525E+14,  2.01239E+14,  1.88741E+14, &
  2.87315E+14,  3.28869E+14,  3.28026E+14,  4.48322E+14,  5.71656E+14, &
  4.59124E+14,  8.54128E+14,  6.48858E+14,  1.12021E+15,  8.98535E+14, &
  1.52389E+15,  1.14453E+15,  2.14281E+15,  1.47268E+15,  2.54409E+15, &
  2.65665E+15,  2.55740E+15,  4.07683E+15,  3.01751E+15,  6.48572E+15, &
 -3.24286E+15 &
         /)

         n = size(densities)
         h = 0.0_dp
        
         klo=max(min(locate(densities,rho),n-1),1)
        
         khi=klo+1
         h=densities(khi)-densities(klo)
	
         if ((h .eq. 0.0) .or. (rho .lt. 1.00000E+09 )) then
            ! print *,'bad density input in M3Y_Rate'
            M3Y_Rate = 0.0_dp
         else
            a=(densities(khi)-rho)/h
            b=(rho-densities(klo))/h

            select case  (pycnoType)
               case (0)
                  ratelo = ratesM3Y_SPVH(klo)
                  ratehi = ratesM3Y_SPVH(khi)
                  derivs2lo = derivs2M3Y_SPVH(klo)
                  derivs2hi = derivs2M3Y_SPVH(khi)
               case (1)
                  ratelo = ratesM3Y_bcc_static(klo)
                  ratehi = ratesM3Y_bcc_static(khi)
                  derivs2lo = derivs2M3Y_bcc_static(klo)
                  derivs2hi = derivs2M3Y_bcc_static(khi)
               case (2)
                  ratelo = ratesM3Y_bcc_ws(klo)
                  ratehi = ratesM3Y_bcc_ws(khi)
                  derivs2lo = derivs2M3Y_bcc_ws(klo)
                  derivs2hi = derivs2M3Y_bcc_ws(khi)
               case (3)
                  ratelo = ratesM3Y_bcc_relaxed(klo)
                  ratehi = ratesM3Y_bcc_relaxed(khi)
                  derivs2lo = derivs2M3Y_bcc_relaxed(klo)
                  derivs2hi = derivs2M3Y_bcc_relaxed(khi)
               case (4)
                  ratelo = ratesM3Y_fcc_static(klo)
                  ratehi = ratesM3Y_fcc_static(khi)
                  derivs2lo = derivs2M3Y_fcc_static(klo)
                  derivs2hi = derivs2M3Y_fcc_static(khi)
               case (5)
                  ratelo = ratesM3Y_fcc_ws(klo)
                  ratehi = ratesM3Y_fcc_ws(khi)
                  derivs2lo = derivs2M3Y_fcc_ws(klo)
                  derivs2hi = derivs2M3Y_fcc_ws(khi)
               case (6)
                  ratelo = ratesM3Y_fcc_relaxed(klo)
                  ratehi = ratesM3Y_fcc_relaxed(khi)
                  derivs2lo = derivs2M3Y_fcc_relaxed(klo)
                  derivs2hi = derivs2M3Y_fcc_relaxed(khi)
            end select

            M3Y_Rate = a*ratelo+b*ratehi+((a**3.0_dp-a)*derivs2lo+ & 
                        (b**3.0_dp-b)*derivs2hi)*(h**2.0_dp)/6.0_dp
         end if
    
          
      end function M3Y_Rate

      real(dp) function SaoPaulo_Rate(rho, pycnoType)
      
         implicit none
      
         real(dp), intent(in) :: rho
         integer,  intent(in) :: pycnoType
         
         integer, parameter   :: NsplinePts = 101
         integer              :: khi,klo,n
         real(dp)             :: a,b
         real(dp)             :: h
         real(dp)             :: ratehi, ratelo, derivs2hi, derivs2lo


         real(dp),  dimension(NsplinePts) :: densities = (/ &
  1.00000E+09,  1.99000E+09,  2.98000E+09,  3.97000E+09,  4.96000E+09, &
  5.95000E+09,  6.94000E+09,  7.93000E+09,  8.92000E+09,  9.91000E+09, &
  1.09000E+10,  1.18900E+10,  1.28800E+10,  1.38700E+10,  1.48600E+10, &
  1.58500E+10,  1.68400E+10,  1.78300E+10,  1.88200E+10,  1.98100E+10, &
  2.08000E+10,  2.17900E+10,  2.27800E+10,  2.37700E+10,  2.47600E+10, &
  2.57500E+10,  2.67400E+10,  2.77300E+10,  2.87200E+10,  2.97100E+10, &
  3.07000E+10,  3.16900E+10,  3.26800E+10,  3.36700E+10,  3.46600E+10, &
  3.56500E+10,  3.66400E+10,  3.76300E+10,  3.86200E+10,  3.96100E+10, &
  4.06000E+10,  4.15900E+10,  4.25800E+10,  4.35700E+10,  4.45600E+10, &
  4.55500E+10,  4.65400E+10,  4.75300E+10,  4.85200E+10,  4.95100E+10, &
  5.05000E+10,  5.14900E+10,  5.24800E+10,  5.34700E+10,  5.44600E+10, &
  5.54500E+10,  5.64400E+10,  5.74300E+10,  5.84200E+10,  5.94100E+10, &
  6.04000E+10,  6.13900E+10,  6.23800E+10,  6.33700E+10,  6.43600E+10, &
  6.53500E+10,  6.63400E+10,  6.73300E+10,  6.83200E+10,  6.93100E+10, &
  7.03000E+10,  7.12900E+10,  7.22800E+10,  7.32700E+10,  7.42600E+10, &
  7.52500E+10,  7.62400E+10,  7.72300E+10,  7.82200E+10,  7.92100E+10, &
  8.02000E+10,  8.11900E+10,  8.21800E+10,  8.31700E+10,  8.41600E+10, &
  8.51500E+10,  8.61400E+10,  8.71300E+10,  8.81200E+10,  8.91100E+10, &
  9.01000E+10,  9.10900E+10,  9.20800E+10,  9.30700E+10,  9.40600E+10, &
  9.50500E+10,  9.60400E+10,  9.70300E+10,  9.80200E+10,  9.90100E+10, &
  1.00000E+11 &
         /)
         
         real(dp),  dimension(NsplinePts) :: ratesSaoPaulo_SPVH = (/ &
  8.03256E-16,  1.65871E-06,  1.71012E-01,  4.05505E+02,  1.33177E+05, &
  1.29813E+07,  5.64532E+08,  1.37931E+10,  2.18730E+11,  2.48416E+12, &
  2.16389E+13,  1.51788E+14,  8.90447E+14,  4.49217E+15,  1.99291E+16, &
  7.92015E+16,  2.85526E+17,  9.47790E+17,  2.91460E+18,  8.40052E+18, &
  2.28029E+19,  5.86521E+19,  1.43691E+20,  3.37127E+20,  7.59891E+20, &
  1.65068E+21,  3.46893E+21,  7.05990E+21,  1.39683E+22,  2.69229E+22, &
  5.05723E+22,  9.28811E+22,  1.66930E+23,  2.94180E+23,  5.08239E+23, &
  8.63464E+23,  1.44226E+24,  2.36908E+24,  3.83684E+24,  6.13404E+24, &
  9.65611E+24,  1.50202E+25,  2.30925E+25,  3.51003E+25,  5.27798E+25, &
  7.85787E+25,  1.15811E+26,  1.68969E+26,  2.44524E+26,  3.50770E+26, &
  4.98728E+26,  7.03810E+26,  9.85912E+26,  1.37103E+27,  1.89316E+27, &
  2.59684E+27,  3.54013E+27,  4.79827E+27,  6.45490E+27,  8.64145E+27, &
  1.15187E+28,  1.52304E+28,  2.00768E+28,  2.63035E+28,  3.42941E+28, &
  4.45943E+28,  5.75834E+28,  7.40587E+28,  9.48733E+28,  1.21067E+29, &
  1.53897E+29,  1.94879E+29,  2.45817E+29,  3.08891E+29,  3.86758E+29, &
  4.82630E+29,  6.00355E+29,  7.44541E+29,  9.20695E+29,  1.13539E+30, &
  1.39235E+30,  1.70585E+30,  2.08544E+30,  2.53749E+30,  3.08296E+30, &
  3.73997E+30,  4.51281E+30,  5.44536E+30,  6.54201E+30,  7.85457E+30, &
  9.39389E+30,  1.12285E+31,  1.33669E+31,  1.59156E+31,  1.88601E+31, &
  2.23368E+31,  2.64042E+31,  3.11150E+31,  3.66538E+31,  4.30602E+31, &
  5.04583E+31 &
        /)
         
         real(dp),  dimension(NsplinePts) :: ratesSaoPaulo_bcc_static = (/ &
  3.14533E-14,  6.58646E-05,  6.84164E+00,  1.63046E+04,  5.37484E+06, &
  5.25453E+08,  2.29065E+10,  5.60821E+11,  8.90926E+12,  1.01342E+14, &
  8.83987E+14,  6.20856E+15,  3.64629E+16,  1.84140E+17,  8.17698E+17, &
  3.25252E+18,  1.17351E+19,  3.89839E+19,  1.19967E+20,  3.46005E+20, &
  9.39815E+20,  2.41878E+21,  5.92912E+21,  1.39184E+22,  3.13885E+22, &
  6.82174E+22,  1.43428E+23,  2.92033E+23,  5.78048E+23,  1.11461E+24, &
  2.09453E+24,  3.84830E+24,  6.91890E+24,  1.21975E+25,  2.10803E+25, &
  3.58259E+25,  5.98603E+25,  9.83582E+25,  1.59344E+26,  2.54822E+26, &
  4.01252E+26,  6.24329E+26,  9.60119E+26,  1.45975E+27,  2.19557E+27, &
  3.26960E+27,  4.81997E+27,  7.03404E+27,  1.01817E+28,  1.46090E+28, &
  2.07758E+28,  2.93254E+28,  4.10884E+28,  5.71504E+28,  7.89307E+28, &
  1.08290E+29,  1.47655E+29,  2.00169E+29,  2.69329E+29,  3.60628E+29, &
  4.80790E+29,  6.35828E+29,  8.38295E+29,  1.09847E+30,  1.43241E+30, &
  1.86294E+30,  2.40595E+30,  3.09482E+30,  3.96525E+30,  5.06077E+30, &
  6.43411E+30,  8.14866E+30,  1.02801E+31,  1.29197E+31,  1.61789E+31, &
  2.01922E+31,  2.51210E+31,  3.11584E+31,  3.85354E+31,  4.75278E+31, &
  5.82916E+31,  7.14254E+31,  8.73300E+31,  1.06273E+32,  1.29134E+32, &
  1.56672E+32,  1.89070E+32,  2.28167E+32,  2.74150E+32,  3.29191E+32, &
  3.93750E+32,  4.70699E+32,  5.60404E+32,  6.67328E+32,  7.90876E+32, &
  9.36767E+32,  1.10746E+33,  1.30518E+33,  1.53767E+33,  1.80661E+33, &
  2.11720E+33 &
        /)

         real(dp),  dimension(NsplinePts) :: ratesSaoPaulo_bcc_ws = (/ &
  3.30550E-10,  2.67562E-01,  1.67053E+04,  2.82978E+07,  7.23645E+09, &
  5.78816E+11,  2.13941E+13,  4.55542E+14,  6.41453E+15,  6.56315E+16, &
  5.21007E+17,  3.36191E+18,  1.82834E+19,  8.60661E+19,  3.58259E+20, &
  1.34229E+21,  4.58104E+21,  1.44482E+22,  4.23505E+22,  1.16682E+23, &
  3.03541E+23,  7.49958E+23,  1.76853E+24,  4.00153E+24,  8.71324E+24, &
  1.83135E+25,  3.72918E+25,  7.36383E+25,  1.41537E+26,  2.65319E+26, &
  4.85220E+26,  8.68485E+26,  1.52257E+27,  2.61964E+27,  4.42213E+27, &
  7.34638E+27,  1.20074E+28,  1.93132E+28,  3.06472E+28,  4.80363E+28, &
  7.41784E+28,  1.13250E+29,  1.70978E+29,  2.55329E+29,  3.77380E+29, &
  5.52495E+29,  8.01064E+29,  1.15025E+30,  1.63887E+30,  2.31548E+30, &
  3.24361E+30,  4.51141E+30,  6.23058E+30,  8.54486E+30,  1.16397E+31, &
  1.57550E+31,  2.11998E+31,  2.83694E+31,  3.76894E+31,  4.98410E+31, &
  6.56414E+31,  8.57744E+31,  1.11765E+32,  1.44773E+32,  1.86656E+32, &
  2.40070E+32,  3.06672E+32,  3.90259E+32,  4.94765E+32,  6.24933E+32, &
  7.86444E+32,  9.86051E+32,  1.23173E+33,  1.53300E+33,  1.90142E+33, &
  2.35080E+33,  2.89757E+33,  3.56122E+33,  4.36485E+33,  5.33580E+33, &
  6.48718E+33,  7.88053E+33,  9.55371E+33,  1.15289E+34,  1.38936E+34, &
  1.67194E+34,  2.00150E+34,  2.39628E+34,  2.85672E+34,  3.40384E+34, &
  4.04040E+34,  4.79373E+34,  5.66498E+34,  6.69645E+34,  7.87881E+34, &
  9.26550E+34,  1.08765E+35,  1.27288E+35,  1.48929E+35,  1.73784E+35, &
  2.02290E+35 &
        /)
        
         real(dp),  dimension(NsplinePts) :: ratesSaoPaulo_bcc_relaxed = (/ &
  2.96024E-10,  2.41474E-01,  1.51390E+04,  2.57158E+07,  6.58978E+09, &
  5.27952E+11,  1.95402E+13,  4.16541E+14,  5.87111E+15,  6.01231E+16, &
  4.77646E+17,  3.08423E+18,  1.67838E+19,  7.90520E+19,  3.29235E+20, &
  1.23415E+21,  4.21387E+21,  1.32958E+22,  3.89879E+22,  1.07458E+23, &
  2.79643E+23,  6.91142E+23,  1.63035E+24,  3.68997E+24,  8.03712E+24, &
  1.68970E+25,  3.44163E+25,  6.79773E+25,  1.30688E+26,  2.45037E+26, &
  4.48227E+26,  8.02442E+26,  1.40708E+27,  2.42141E+27,  4.08829E+27, &
  6.79302E+27,  1.11050E+28,  1.78647E+28,  2.83535E+28,  4.44484E+28, &
  6.86488E+28,  1.04824E+29,  1.58281E+29,  2.36403E+29,  3.49456E+29, &
  5.11684E+29,  7.41992E+29,  1.06557E+30,  1.51841E+30,  2.14555E+30, &
  3.00594E+30,  4.18136E+30,  5.77542E+30,  7.92155E+30,  1.07918E+31, &
  1.46090E+31,  1.96598E+31,  2.63114E+31,  3.49589E+31,  4.62349E+31, &
  6.08981E+31,  7.95841E+31,  1.03709E+32,  1.34350E+32,  1.73234E+32, &
  2.22827E+32,  2.84671E+32,  3.62293E+32,  4.59349E+32,  5.80248E+32, &
  7.30271E+32,  9.15696E+32,  1.14394E+33,  1.42385E+33,  1.76617E+33, &
  2.18375E+33,  2.69188E+33,  3.30866E+33,  4.05559E+33,  4.95811E+33, &
  6.02842E+33,  7.32375E+33,  8.87933E+33,  1.07159E+34,  1.29146E+34, &
  1.55424E+34,  1.86072E+34,  2.22787E+34,  2.65612E+34,  3.16502E+34, &
  3.75716E+34,  4.45794E+34,  5.26848E+34,  6.22813E+34,  7.32823E+34, &
  8.61852E+34,  1.01176E+35,  1.18414E+35,  1.38553E+35,  1.61686E+35, &
  1.88218E+35 &
        /)
        
         real(dp),  dimension(NsplinePts) :: ratesSaoPaulo_fcc_static = (/ &
  9.89497E-16,  3.19482E-06,  4.18471E-01,  1.16506E+03,  4.31164E+05, &
  4.61863E+07,  2.17064E+09,  5.66334E+10,  9.50498E+11,  1.13462E+13, &
  1.03313E+14,  7.54160E+14,  4.58706E+15,  2.39185E+16,  1.09388E+17, &
  4.47128E+17,  1.65463E+18,  5.62821E+18,  1.77082E+19,  5.21492E+19, &
  1.44460E+20,  3.78774E+20,  9.45007E+20,  2.25588E+21,  5.16934E+21, &
  1.14072E+22,  2.43359E+22,  5.02468E+22,  1.00799E+23,  1.96879E+23, &
  3.74574E+23,  6.96455E+23,  1.26663E+24,  2.25787E+24,  3.94419E+24, &
  6.77299E+24,  1.14308E+25,  1.89658E+25,  3.10165E+25,  5.00572E+25, &
  7.95255E+25,  1.24811E+26,  1.93559E+26,  2.96701E+26,  4.49827E+26, &
  6.75089E+26,  1.00276E+27,  1.47422E+27,  2.14934E+27,  3.10569E+27, &
  4.44713E+27,  6.31948E+27,  8.91266E+27,  1.24766E+28,  1.73402E+28, &
  2.39370E+28,  3.28357E+28,  4.47774E+28,  6.05979E+28,  8.16016E+28, &
  1.09398E+29,  1.45468E+29,  1.92819E+29,  2.53996E+29,  3.32926E+29, &
  4.35193E+29,  5.64850E+29,  7.30143E+29,  9.40013E+29,  1.20541E+30, &
  1.53968E+30,  1.95892E+30,  2.48246E+30,  3.13375E+30,  3.94145E+30, &
  4.94034E+30,  6.17232E+30,  7.68771E+30,  9.54697E+30,  1.18225E+31, &
  1.45580E+31,  1.79083E+31,  2.19810E+31,  2.68515E+31,  3.27507E+31, &
  3.98830E+31,  4.83070E+31,  5.85076E+31,  7.05500E+31,  8.50135E+31, &
  1.02040E+32,  1.22401E+32,  1.46223E+32,  1.74706E+32,  2.07737E+32, &
  2.46863E+32,  2.92788E+32,  3.46162E+32,  4.09112E+32,  4.82165E+32, &
  5.66802E+32 &
        /)
        
         real(dp),  dimension(NsplinePts) :: ratesSaoPaulo_fcc_ws = (/ &
  9.60238E-11,  9.52987E-02,  6.63630E+03,  1.20953E+07,  3.26617E+09, &
  2.72736E+11,  1.04440E+13,  2.29141E+14,  3.31109E+15,  3.46564E+16, &
  2.80731E+17,  1.84470E+18,  1.01990E+19,  4.87390E+19,  2.05714E+20, &
  7.80698E+20,  2.69637E+21,  8.59933E+21,  2.54707E+22,  7.08679E+22, &
  1.86072E+23,  4.63772E+23,  1.10278E+24,  2.51494E+24,  5.51756E+24, &
  1.16803E+25,  2.39484E+25,  4.76016E+25,  9.20717E+25,  1.73641E+26, &
  3.19413E+26,  5.74926E+26,  1.01339E+27,  1.75269E+27,  2.97363E+27, &
  4.96418E+27,  8.15220E+27,  1.31724E+28,  2.09957E+28,  3.30506E+28, &
  5.12513E+28,  7.85655E+28,  1.19084E+29,  1.78518E+29,  2.64843E+29, &
  3.89157E+29,  5.66252E+29,  8.15915E+29,  1.16645E+30,  1.65349E+30, &
  2.32377E+30,  3.24228E+30,  4.49168E+30,  6.17872E+30,  8.44148E+30, &
  1.14592E+31,  1.54632E+31,  2.07504E+31,  2.76425E+31,  3.66527E+31, &
  4.83987E+31,  6.34060E+31,  8.28276E+31,  1.07555E+32,  1.39008E+32, &
  1.79214E+32,  2.29471E+32,  2.92691E+32,  3.71911E+32,  4.70804E+32, &
  5.93781E+32,  7.46095E+32,  9.33965E+32,  1.16484E+33,  1.44775E+33, &
  1.79353E+33,  2.21510E+33,  2.72778E+33,  3.34980E+33,  4.10277E+33, &
  4.99745E+33,  6.08207E+33,  7.38687E+33,  8.93017E+33,  1.07809E+34, &
  1.29964E+34,  1.55850E+34,  1.86908E+34,  2.23197E+34,  2.66384E+34, &
  3.16720E+34,  3.76380E+34,  4.45497E+34,  5.27443E+34,  6.21538E+34, &
  7.32055E+34,  8.60641E+34,  1.00873E+35,  1.18198E+35,  1.38127E+35, &
  1.61017E+35 &
        /)
        
         real(dp),  dimension(NsplinePts) :: ratesSaoPaulo_fcc_relaxed = (/ &
  1.33600E-10,  1.26964E-01,  8.63835E+03,  1.55009E+07,  4.13758E+09, &
  3.42353E+11,  1.30115E+13,  2.83659E+14,  4.07639E+15,  4.24609E+16, &
  3.42475E+17,  2.24174E+18,  1.23507E+19,  5.88328E+19,  2.47585E+20, &
  9.37043E+20,  3.22815E+21,  1.02709E+22,  3.03544E+22,  8.42799E+22, &
  2.20852E+23,  5.49433E+23,  1.30416E+24,  2.96921E+24,  6.50378E+24, &
  1.37471E+25,  2.81448E+25,  5.58644E+25,  1.07909E+26,  2.03247E+26, &
  3.73410E+26,  6.71316E+26,  1.18193E+27,  2.04192E+27,  3.46063E+27, &
  5.77117E+27,  9.46793E+27,  1.52835E+28,  2.43376E+28,  3.82762E+28, &
  5.93017E+28,  9.08278E+28,  1.37554E+29,  2.06039E+29,  3.05428E+29, &
  4.48442E+29,  6.52023E+29,  9.38808E+29,  1.34118E+30,  1.89983E+30, &
  2.66813E+30,  3.72025E+30,  5.15045E+30,  7.08036E+30,  9.66723E+30, &
  1.31151E+31,  1.76870E+31,  2.37204E+31,  3.15808E+31,  4.18508E+31, &
  5.52319E+31,  7.23184E+31,  9.44193E+31,  1.22542E+32,  1.58297E+32, &
  2.03979E+32,  2.61050E+32,  3.32805E+32,  4.22679E+32,  5.34819E+32, &
  6.74203E+32,  8.46758E+32,  1.05950E+33,  1.32081E+33,  1.64089E+33, &
  2.03193E+33,  2.50846E+33,  3.08776E+33,  3.79031E+33,  4.64040E+33, &
  5.65007E+33,  6.87363E+33,  8.34500E+33,  1.00846E+34,  1.21700E+34, &
  1.46655E+34,  1.75801E+34,  2.10758E+34,  2.51588E+34,  3.00163E+34, &
  3.56758E+34,  4.23813E+34,  5.01470E+34,  5.93513E+34,  6.99163E+34, &
  8.23214E+34,  9.67499E+34,  1.13362E+35,  1.32789E+35,  1.55130E+35, &
  1.80782E+35 &
        /)

         real(dp),  dimension(NsplinePts) :: derivs2SaoPaulo_SPVH = (/ &
   7.17394E-12, -1.43479E-11,  5.02176E-11, -1.86520E-10,  6.96672E-10, &
 -2.52233E-09,  1.26905E-08,  2.93669E-08,  1.04345E-06,  8.41080E-06, &
  6.87067E-05,  3.96253E-04,  2.07147E-03,  8.84505E-03,  3.50015E-02, &
  1.19501E-01,  3.87221E-01,  1.12279E+00,  3.10780E+00,  7.98948E+00, &
  1.95193E+01,  4.52266E+01,  1.00707E+02,  2.15532E+02,  4.41073E+02, &
  8.85315E+02,  1.69548E+03,  3.18504E+03,  5.87295E+03,  1.03372E+04, &
  1.82497E+04,  3.08935E+04,  5.24835E+04,  8.48598E+04,  1.39509E+05, &
  2.21292E+05,  3.44009E+05,  5.33179E+05,  8.34815E+05,  1.20525E+06, &
  1.84263E+06,  2.70101E+06,  3.93198E+06,  5.66361E+06,  8.13506E+06, &
  1.15015E+07,  1.58506E+07,  2.25916E+07,  3.08959E+07,  4.17095E+07, &
  5.76180E+07,  7.75190E+07,  1.03814E+08,  1.37893E+08,  1.83332E+08, &
  2.40220E+08,  3.22660E+08,  3.96556E+08,  5.30635E+08,  7.25007E+08, &
  7.97737E+08,  1.19240E+09,  1.37875E+09,  1.74289E+09,  2.44767E+09, &
  2.60590E+09,  3.58916E+09,  4.38006E+09,  5.45467E+09,  6.72801E+09, &
  8.26774E+09,  1.00967E+10,  1.22999E+10,  1.49976E+10,  1.82727E+10, &
  2.21303E+10,  2.69905E+10,  3.18943E+10,  4.11366E+10,  3.95275E+10, &
  5.94499E+10,  6.88036E+10,  6.99329E+10,  9.50731E+10,  1.21663E+11, &
  1.01090E+11,  1.83063E+11,  1.44421E+11,  2.43795E+11,  2.02158E+11, &
  3.35811E+11,  2.61956E+11,  4.76676E+11,  3.42710E+11,  5.76098E+11, &
  6.10574E+11,  5.97126E+11,  9.40416E+11,  7.09956E+11,  1.53146E+12, &
 -7.65732E+11 &
        /)

         real(dp),  dimension(NsplinePts) :: derivs2SaoPaulo_bcc_static = (/ &
  2.95001E-10, -5.90003E-10,  2.06501E-09, -7.66994E-09,  2.86474E-08, &
 -1.03769E-07,  5.20257E-07,  1.17875E-06,  4.25794E-05,  3.43251E-04, &
  2.80978E-03,  1.62225E-02,  8.49158E-02,  3.62955E-01,  1.43774E+00, &
  4.91310E+00,  1.59333E+01,  4.62368E+01,  1.28072E+02,  3.29474E+02, &
  8.05464E+02,  1.86742E+03,  4.16059E+03,  8.90939E+03,  1.82420E+04, &
  3.66331E+04,  7.01905E+04,  1.31915E+05,  2.43346E+05,  4.28507E+05, &
  7.56799E+05,  1.28163E+06,  2.17809E+06,  3.52305E+06,  5.79378E+06, &
  9.19338E+06,  1.42963E+07,  2.21646E+07,  3.47137E+07,  5.01346E+07, &
  7.66668E+07,  1.12414E+08,  1.63691E+08,  2.35842E+08,  3.38843E+08, &
  4.79184E+08,  6.60553E+08,  9.41670E+08,  1.28813E+09,  1.73940E+09, &
  2.40331E+09,  3.23412E+09,  4.33208E+09,  5.75538E+09,  7.65344E+09, &
  1.00303E+10,  1.34749E+10,  1.65653E+10,  2.21690E+10,  3.02936E+10, &
  3.33446E+10,  4.98395E+10,  5.76462E+10,  7.28814E+10,  1.02358E+11, &
  1.09016E+11,  1.50152E+11,  1.83274E+11,  2.28273E+11,  2.81604E+11, &
  3.46103E+11,  4.22730E+11,  5.15050E+11,  6.28103E+11,  7.65371E+11, &
  9.27076E+11,  1.13083E+12,  1.33648E+12,  1.72387E+12,  1.65712E+12, &
  2.49185E+12,  2.88446E+12,  2.93274E+12,  3.98671E+12,  5.10205E+12, &
  4.24170E+12,  7.67790E+12,  6.06101E+12,  1.02278E+13,  8.48574E+12, &
  1.40912E+13,  1.09991E+13,  2.00055E+13,  1.43939E+13,  2.41855E+13, &
  2.56393E+13,  2.50813E+13,  3.94929E+13,  2.98246E+13,  6.43398E+13, &
 -3.21699E+13 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2SaoPaulo_bcc_ws = (/ &
  1.10965E-07, -2.21929E-07,  7.76752E-07, -2.88491E-06,  1.08068E-05, &
 -3.68874E-05,  2.60671E-04,  1.52455E-03,  2.74632E-02,  2.14659E-01, &
  1.53912E+00,  8.23265E+00,  3.94855E+01,  1.57432E+02,  5.82148E+02, &
  1.87172E+03,  5.73395E+03,  1.57705E+04,  4.15920E+04,  1.02095E+05, &
  2.38897E+05,  5.31287E+05,  1.13860E+06,  2.34875E+06,  4.64073E+06, &
  9.01490E+06,  1.67106E+07,  3.04678E+07,  5.45768E+07,  9.33300E+07, &
  1.60528E+08,  2.64640E+08,  4.38856E+08,  6.91746E+08,  1.11269E+09, &
  1.72462E+09,  2.62119E+09,  3.98104E+09,  6.11544E+09,  8.62471E+09, &
  1.29703E+10,  1.86470E+10,  2.66542E+10,  3.77157E+10,  5.32745E+10, &
  7.40365E+10,  1.00252E+11,  1.40939E+11,  1.89535E+11,  2.51767E+11, &
  3.43195E+11,  4.54866E+11,  6.00481E+11,  7.86458E+11,  1.03180E+12, &
  1.33384E+12,  1.77165E+12,  2.13862E+12,  2.83816E+12,  3.84356E+12, &
  4.12414E+12,  6.18414E+12,  7.00076E+12,  8.76544E+12,  1.22730E+13, &
  1.27330E+13,  1.75313E+13,  2.11212E+13,  2.60450E+13,  3.17916E+13, &
  3.86707E+13,  4.67435E+13,  5.63802E+13,  6.81040E+13,  8.22215E+13, &
  9.86700E+13,  1.19318E+14,  1.39560E+14,  1.79335E+14,  1.67433E+14, &
  2.55501E+14,  2.91864E+14,  2.90097E+14,  3.96930E+14,  5.05824E+14, &
  4.03657E+14,  7.54890E+14,  5.69415E+14,  9.87370E+14,  7.87076E+14, &
  1.34025E+15,  9.99487E+14,  1.88154E+15,  1.28224E+15,  2.22684E+15, &
  2.31910E+15,  2.22603E+15,  3.55701E+15,  2.62315E+15,  5.63511E+15, &
 -2.81756E+15 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2SaoPaulo_bcc_relaxed = (/ &
  1.02104E-07, -2.04207E-07,  7.14725E-07, -2.65454E-06,  9.94345E-06, &
 -3.39677E-05,  2.39126E-04,  1.39144E-03,  2.51567E-02,  1.96711E-01, &
  1.41188E+00,  7.55687E+00,  3.62697E+01,  1.44693E+02,  5.35340E+02, &
  1.72208E+03,  5.27795E+03,  1.45227E+04,  3.83164E+04,  9.40904E+04, &
  2.20245E+05,  4.89971E+05,  1.05039E+06,  2.16744E+06,  4.28373E+06, &
  8.32366E+06,  1.54335E+07,  2.81461E+07,  5.04300E+07,  8.62593E+07, &
  1.48398E+08,  2.44697E+08,  4.05865E+08,  6.39878E+08,  1.02945E+09, &
  1.59589E+09,  2.42600E+09,  3.68520E+09,  5.66188E+09,  7.98661E+09, &
  1.20123E+10,  1.72725E+10,  2.46932E+10,  3.49460E+10,  4.93690E+10, &
  6.86187E+10,  9.29290E+10,  1.30659E+11,  1.75735E+11,  2.33467E+11, &
  3.18285E+11,  4.21902E+11,  5.57031E+11,  7.29636E+11,  9.57357E+11, &
  1.23774E+12,  1.64416E+12,  1.98502E+12,  2.63450E+12,  3.56800E+12, &
  3.82929E+12,  5.74181E+12,  6.50119E+12,  8.14058E+12,  1.13983E+13, &
  1.18281E+13,  1.62854E+13,  1.96223E+13,  2.41986E+13,  2.95404E+13, &
  3.59354E+13,  4.34409E+13,  5.24011E+13,  6.33023E+13,  7.64302E+13, &
  9.17271E+13,  1.10930E+14,  1.29760E+14,  1.66747E+14,  1.55719E+14, &
  2.37593E+14,  2.71438E+14,  2.69847E+14,  3.69200E+14,  4.70501E+14, &
  3.75602E+14,  7.02220E+14,  5.29893E+14,  9.18616E+14,  7.32521E+14, &
  1.24708E+15,  9.30368E+14,  1.75089E+15,  1.19377E+15,  2.07258E+15, &
  2.15877E+15,  2.07247E+15,  3.31120E+15,  2.44237E+15,  5.24691E+15, &
 -2.62346E+15 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2SaoPaulo_fcc_static = (/ &
  4.22542E-11, -8.45084E-11,  2.95779E-10, -1.09860E-09,  4.10125E-09, &
 -1.50289E-08,  6.87399E-08,  6.04751E-08,  4.82803E-06,  3.87963E-05, &
  3.39348E-04,  2.02518E-03,  1.10399E-02,  4.86950E-02,  1.99068E-01, &
  6.99388E-01,  2.32792E+00,  6.92236E+00,  1.96086E+01,  5.15337E+01, &
  1.28525E+02,  3.03684E+02,  6.88696E+02,  1.50008E+03,  3.12170E+03, &
  6.36462E+03,  1.23799E+04,  2.35908E+04,  4.41054E+04,  7.87055E+04, &
  1.40696E+05,  2.41198E+05,  4.14527E+05,  6.78396E+05,  1.12703E+06, &
  1.80751E+06,  2.84019E+06,  4.44499E+06,  7.02372E+06,  1.02527E+07, &
  1.58006E+07,  2.33760E+07,  3.43274E+07,  4.98665E+07,  7.22031E+07, &
  1.02921E+08,  1.43029E+08,  2.05218E+08,  2.82896E+08,  3.84856E+08, &
  5.35070E+08,  7.25032E+08,  9.77632E+08,  1.30730E+09,  1.74918E+09, &
  2.30676E+09,  3.11524E+09,  3.86080E+09,  5.18762E+09,  7.11881E+09, &
  7.92284E+09,  1.18326E+10,  1.38148E+10,  1.75451E+10,  2.46835E+10, &
  2.65879E+10,  3.66442E+10,  4.49893E+10,  5.62931E+10,  6.97798E+10, &
  8.61673E+10,  1.05743E+11,  1.29429E+11,  1.58526E+11,  1.93992E+11, &
  2.35978E+11,  2.88985E+11,  3.43144E+11,  4.43458E+11,  4.31599E+11, &
  6.45353E+11,  7.51400E+11,  7.71436E+11,  1.04627E+12,  1.34174E+12, &
  1.13476E+12,  2.02741E+12,  1.63127E+12,  2.72311E+12,  2.29757E+12, &
  3.77726E+12,  3.00516E+12,  5.39079E+12,  3.96724E+12,  6.57919E+12, &
  7.02834E+12,  6.93125E+12,  1.08516E+13,  8.27712E+12,  1.78960E+13, &
 -8.94799E+12 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2SaoPaulo_fcc_ws = (/ &
  6.58613E-08, -1.31723E-07,  4.61029E-07, -1.71232E-06,  6.40810E-06, &
 -2.22904E-05,  1.43370E-04,  7.25366E-04,  1.44835E-02,  1.14364E-01, &
  8.42599E-01,  4.58314E+00,  2.23939E+01,  9.06332E+01,  3.40109E+02, &
  1.10791E+03,  3.43570E+03,  9.55877E+03,  2.54759E+04,  6.31675E+04, &
  1.49203E+05,  3.34788E+05,  7.23485E+05,  1.50443E+06,  2.99530E+06, &
  5.86011E+06,  1.09401E+07,  2.00767E+07,  3.61904E+07,  6.22787E+07, &
  1.07728E+08,  1.78624E+08,  2.97773E+08,  4.71978E+08,  7.62790E+08, &
  1.18826E+09,  1.81490E+09,  2.76862E+09,  4.27062E+09,  6.05395E+09, &
  9.13664E+09,  1.31912E+10,  1.89307E+10,  2.68908E+10,  3.81225E+10, &
  5.31784E+10,  7.22853E+10,  1.01924E+11,  1.37571E+11,  1.83388E+11, &
  2.50705E+11,  3.33368E+11,  4.41465E+11,  5.79967E+11,  7.63096E+11, &
  9.89380E+11,  1.31731E+12,  1.59649E+12,  2.12241E+12,  2.87972E+12, &
  3.10736E+12,  4.65543E+12,  5.29511E+12,  6.64349E+12,  9.30723E+12, &
  9.71221E+12,  1.33715E+13,  1.61551E+13,  1.99639E+13,  2.44242E+13, &
  2.97752E+13,  3.60716E+13,  4.36024E+13,  5.27767E+13,  6.38436E+13, &
  7.67686E+13,  9.30052E+13,  1.09028E+14,  1.40206E+14,  1.31756E+14, &
  2.00367E+14,  2.29548E+14,  2.29308E+14,  3.13259E+14,  3.99549E+14, &
  3.21795E+14,  5.97303E+14,  4.55171E+14,  7.84340E+14,  6.30863E+14, &
  1.06806E+15,  8.04711E+14,  1.50297E+15,  1.03686E+15,  1.78703E+15, &
  1.86840E+15,  1.80098E+15,  2.86801E+15,  2.12633E+15,  4.57155E+15, &
 -2.28578E+15 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2SaoPaulo_fcc_relaxed = (/ &
  7.87122E-08, -1.57424E-07,  5.50985E-07, -2.04642E-06,  7.65984E-06, &
 -2.65477E-05,  1.74018E-04,  9.09773E-04,  1.77485E-02,  1.39861E-01, &
  1.02446E+00,  5.55260E+00,  2.70235E+01,  1.09023E+02,  4.07839E+02, &
  1.32485E+03,  4.09778E+03,  1.13730E+04,  3.02434E+04,  7.48280E+04, &
  1.76392E+05,  3.95047E+05,  8.52194E+05,  1.76905E+06,  3.51644E+06, &
  6.86927E+06,  1.28048E+07,  2.34661E+07,  4.22435E+07,  7.25979E+07, &
  1.25427E+08,  2.07715E+08,  3.45882E+08,  5.47586E+08,  8.84092E+08, &
  1.37575E+09,  2.09906E+09,  3.19911E+09,  4.93031E+09,  6.98149E+09, &
  1.05285E+10,  1.51871E+10,  2.17765E+10,  3.09078E+10,  4.37834E+10, &
  6.10263E+10,  8.28852E+10,  1.16796E+11,  1.57522E+11,  2.09826E+11, &
  2.86672E+11,  3.80929E+11,  5.04113E+11,  6.61838E+11,  8.70282E+11, &
  1.12765E+12,  1.50063E+12,  1.81713E+12,  2.41483E+12,  3.27515E+12, &
  3.52985E+12,  5.28934E+12,  6.01012E+12,  7.53727E+12,  1.05580E+13, &
  1.10040E+13,  1.51500E+13,  1.82929E+13,  2.25954E+13,  2.76303E+13, &
  3.36678E+13,  4.07679E+13,  4.92565E+13,  5.95946E+13,  7.20606E+13, &
  8.66124E+13,  1.04890E+14,  1.22901E+14,  1.58021E+14,  1.48293E+14, &
  2.25678E+14,  2.58386E+14,  2.57843E+14,  3.52356E+14,  4.49331E+14, &
  3.61189E+14,  6.71478E+14,  5.10596E+14,  8.81003E+14,  7.07274E+14, &
  1.19887E+15,  9.01323E+14,  1.68620E+15,  1.16028E+15,  2.00293E+15, &
  2.09238E+15,  2.01510E+15,  3.21127E+15,  2.37816E+15,  5.11206E+15, &
 -2.55603E+15 &
         /)

         n = size(densities)
         h = 0.0_dp
        
         klo=max(min(locate(densities,rho),n-1),1)
        
         khi=klo+1
         h=densities(khi)-densities(klo)

	
         if ((h .eq. 0.0) .or. (rho .lt. 1.00000E+09 )) then
            ! print *,'bad density input in M3Y_Rate'
            SaoPaulo_Rate = 0.0_dp
         else
            a=(densities(khi)-rho)/h
            b=(rho-densities(klo))/h
            select case  (pycnoType)
               case (0)
                  ratelo = ratesSaoPaulo_SPVH(klo)
                  ratehi = ratesSaoPaulo_SPVH(khi)
                  derivs2lo = derivs2SaoPaulo_SPVH(klo)
                  derivs2hi = derivs2SaoPaulo_SPVH(khi)
               case (1)
                  ratelo = ratesSaoPaulo_bcc_static(klo)
                  ratehi = ratesSaoPaulo_bcc_static(khi)
                  derivs2lo = derivs2SaoPaulo_bcc_static(klo)
                  derivs2hi = derivs2SaoPaulo_bcc_static(khi)
               case (2)
                  ratelo = ratesSaoPaulo_bcc_ws(klo)
                  ratehi = ratesSaoPaulo_bcc_ws(khi)
                  derivs2lo = derivs2SaoPaulo_bcc_ws(klo)
                  derivs2hi = derivs2SaoPaulo_bcc_ws(khi)
               case (3)
                  ratelo = ratesSaoPaulo_bcc_relaxed(klo)
                  ratehi = ratesSaoPaulo_bcc_relaxed(khi)
                  derivs2lo = derivs2SaoPaulo_bcc_relaxed(klo)
                  derivs2hi = derivs2SaoPaulo_bcc_relaxed(khi)
               case (4)
                  ratelo = ratesSaoPaulo_fcc_static(klo)
                  ratehi = ratesSaoPaulo_fcc_static(khi)
                  derivs2lo = derivs2SaoPaulo_fcc_static(klo)
                  derivs2hi = derivs2SaoPaulo_fcc_static(khi)
               case (5)
                  ratelo = ratesSaoPaulo_fcc_ws(klo)
                  ratehi = ratesSaoPaulo_fcc_ws(khi)
                  derivs2lo = derivs2SaoPaulo_fcc_ws(klo)
                  derivs2hi = derivs2SaoPaulo_fcc_ws(khi)
               case (6)
                  ratelo = ratesSaoPaulo_fcc_relaxed(klo)
                  ratehi = ratesSaoPaulo_fcc_relaxed(khi)
                  derivs2lo = derivs2SaoPaulo_fcc_relaxed(klo)
                  derivs2hi = derivs2SaoPaulo_fcc_relaxed(khi)
            end select
            SaoPaulo_Rate = a*ratelo+b*ratehi+((a**3.0_dp-a)*derivs2lo+ & 
                        (b**3.0_dp-b)*derivs2hi)*(h**2.0_dp)/6.0_dp
         end if

      end function SaoPaulo_Rate
    
      real(dp) function RMF_Rate(rho, pycnoType)
      
         implicit none
      
         real(dp), intent(in) :: rho
         integer,  intent(in) :: pycnoType 
         
         integer, parameter   :: NsplinePts = 101
         integer              :: khi,klo,n
         real(dp)             :: a,b
         real(dp)             :: h
         real(dp)             :: ratehi, ratelo, derivs2hi, derivs2lo


         real(dp),  dimension(NsplinePts) :: densities = (/ &
  1.00000E+09,  1.99000E+09,  2.98000E+09,  3.97000E+09,  4.96000E+09, &
  5.95000E+09,  6.94000E+09,  7.93000E+09,  8.92000E+09,  9.91000E+09, &
  1.09000E+10,  1.18900E+10,  1.28800E+10,  1.38700E+10,  1.48600E+10, &
  1.58500E+10,  1.68400E+10,  1.78300E+10,  1.88200E+10,  1.98100E+10, &
  2.08000E+10,  2.17900E+10,  2.27800E+10,  2.37700E+10,  2.47600E+10, &
  2.57500E+10,  2.67400E+10,  2.77300E+10,  2.87200E+10,  2.97100E+10, &
  3.07000E+10,  3.16900E+10,  3.26800E+10,  3.36700E+10,  3.46600E+10, &
  3.56500E+10,  3.66400E+10,  3.76300E+10,  3.86200E+10,  3.96100E+10, &
  4.06000E+10,  4.15900E+10,  4.25800E+10,  4.35700E+10,  4.45600E+10, &
  4.55500E+10,  4.65400E+10,  4.75300E+10,  4.85200E+10,  4.95100E+10, &
  5.05000E+10,  5.14900E+10,  5.24800E+10,  5.34700E+10,  5.44600E+10, &
  5.54500E+10,  5.64400E+10,  5.74300E+10,  5.84200E+10,  5.94100E+10, &
  6.04000E+10,  6.13900E+10,  6.23800E+10,  6.33700E+10,  6.43600E+10, &
  6.53500E+10,  6.63400E+10,  6.73300E+10,  6.83200E+10,  6.93100E+10, &
  7.03000E+10,  7.12900E+10,  7.22800E+10,  7.32700E+10,  7.42600E+10, &
  7.52500E+10,  7.62400E+10,  7.72300E+10,  7.82200E+10,  7.92100E+10, &
  8.02000E+10,  8.11900E+10,  8.21800E+10,  8.31700E+10,  8.41600E+10, &
  8.51500E+10,  8.61400E+10,  8.71300E+10,  8.81200E+10,  8.91100E+10, &
  9.01000E+10,  9.10900E+10,  9.20800E+10,  9.30700E+10,  9.40600E+10, &
  9.50500E+10,  9.60400E+10,  9.70300E+10,  9.80200E+10,  9.90100E+10, &
  1.00000E+11 &
         /)
         
         real(dp),  dimension(NsplinePts) :: ratesRMF_SPVH = (/ &  
  1.65929E-15,  3.38599E-06,  3.45278E-01,  8.06988E+02,  2.62205E+05, &
  2.55485E+07,  1.11072E+09,  2.71308E+10,  4.30143E+11,  4.88429E+12, &
  4.25390E+13,  2.98354E+14,  1.75005E+15,  8.82788E+15,  3.91610E+16, &
  1.55622E+17,  5.61000E+17,  1.86215E+18,  5.72625E+18,  1.65042E+19, &
  4.47999E+19,  1.15232E+20,  2.82313E+20,  6.62380E+20,  1.49308E+21, &
  3.24350E+21,  6.81669E+21,  1.38741E+22,  2.74526E+22,  5.29174E+22, &
  9.94097E+22,  1.82594E+23,  3.28202E+23,  5.78456E+23,  9.99492E+23, &
  1.69829E+24,  2.83710E+24,  4.66095E+24,  7.54981E+24,  1.20720E+25, &
  1.90069E+25,  2.95709E+25,  4.54718E+25,  6.91302E+25,  1.03972E+26, &
  1.54827E+26,  2.28239E+26,  3.33080E+26,  4.82137E+26,  6.91804E+26, &
  9.83875E+26,  1.38884E+27,  1.94607E+27,  2.70707E+27,  3.73916E+27, &
  5.13065E+27,  6.99670E+27,  9.48660E+27,  1.27666E+28,  1.70977E+28, &
  2.27996E+28,  3.01588E+28,  3.97725E+28,  5.21313E+28,  6.79999E+28, &
  8.84674E+28,  1.14295E+29,  1.47076E+29,  1.88520E+29,  2.40712E+29, &
  3.06183E+29,  3.87978E+29,  4.89740E+29,  6.15874E+29,  7.71760E+29, &
  9.63919E+29,  1.20019E+30,  1.49001E+30,  1.84468E+30,  2.27783E+30, &
  2.79754E+30,  3.43359E+30,  4.20720E+30,  5.13657E+30,  6.27515E+30, &
  7.60907E+30,  9.17738E+30,  1.10690E+31,  1.32924E+31,  1.59524E+31, &
  1.90705E+31,  2.27851E+31,  2.71129E+31,  3.22689E+31,  3.82229E+31, &
  4.52500E+31,  5.34673E+31,  6.29804E+31,  7.41610E+31,  8.70874E+31, &
  1.02008E+32 &
         /)
         
         real(dp),  dimension(NsplinePts) :: ratesRMF_bcc_static = (/ &
  6.49733E-14,  1.34452E-04,  1.38135E+01,  3.24474E+04,  1.05822E+07, &
  1.03415E+09,  4.50686E+10,  1.10313E+12,  1.75205E+13,  1.99255E+14, &
  1.73779E+15,  1.22035E+16,  7.16628E+16,  3.61867E+17,  1.60679E+18, &
  6.39085E+18,  2.30571E+19,  7.65927E+19,  2.35697E+20,  6.79782E+20, &
  1.84642E+21,  4.75212E+21,  1.16491E+22,  2.73466E+22,  6.16740E+22, &
  1.34044E+23,  2.81845E+23,  5.73903E+23,  1.13607E+24,  2.19078E+24, &
  4.11721E+24,  7.56534E+24,  1.36033E+25,  2.39844E+25,  4.14560E+25, &
  7.04639E+25,  1.17752E+26,  1.93511E+26,  3.13544E+26,  5.01501E+26, &
  7.89818E+26,  1.22914E+27,  1.89058E+27,  2.87499E+27,  4.32510E+27, &
  6.44223E+27,  9.49914E+27,  1.38659E+28,  2.00757E+28,  2.88125E+28, &
  4.09859E+28,  5.78682E+28,  8.11036E+28,  1.12842E+29,  1.55895E+29, &
  2.13953E+29,  2.91825E+29,  3.95752E+29,  5.32682E+29,  7.13526E+29, &
  9.51651E+29,  1.25905E+30,  1.66068E+30,  2.17708E+30,  2.84026E+30, &
  3.69576E+30,  4.77546E+30,  6.14609E+30,  7.87921E+30,  1.00621E+31, &
  1.28009E+31,  1.62229E+31,  2.04810E+31,  2.57596E+31,  3.22842E+31, &
  4.03282E+31,  5.02203E+31,  6.23555E+31,  7.72084E+31,  9.53501E+31, &
  1.17121E+32,  1.43767E+32,  1.76181E+32,  2.15126E+32,  2.62843E+32, &
  3.18754E+32,  3.84498E+32,  4.63804E+32,  5.57032E+32,  6.68578E+32, &
  7.99351E+32,  9.55155E+32,  1.13670E+33,  1.35301E+33,  1.60283E+33, &
  1.89770E+33,  2.24256E+33,  2.64183E+33,  3.11114E+33,  3.65378E+33, &
  4.28020E+33 &
         /)

         real(dp),  dimension(NsplinePts) :: ratesRMF_bcc_ws = (/ &
  6.82821E-10,  5.46185E-01,  3.37285E+04,  5.63148E+07,  1.42474E+10, &
  1.13917E+12,  4.20927E+13,  8.96043E+14,  1.26145E+16,  1.29043E+17, &
  1.02423E+18,  6.60813E+18,  3.59335E+19,  1.69135E+20,  7.03984E+20, &
  2.63745E+21,  9.00080E+21,  2.83867E+22,  8.32052E+22,  2.29241E+23, &
  5.96354E+23,  1.47343E+24,  3.47467E+24,  7.86212E+24,  1.71203E+25, &
  3.59851E+25,  7.32809E+25,  1.44714E+26,  2.78170E+26,  5.21488E+26, &
  9.53795E+26,  1.70735E+27,  2.99354E+27,  5.15108E+27,  8.69647E+27, &
  1.44491E+28,  2.36200E+28,  3.79968E+28,  6.03051E+28,  9.45374E+28, &
  1.46011E+29,  2.22960E+29,  3.36676E+29,  5.02873E+29,  7.43408E+29, &
  1.08861E+30,  1.57873E+30,  2.26744E+30,  3.23142E+30,  4.56668E+30, &
  6.39889E+30,  8.90244E+30,  1.22984E+31,  1.68716E+31,  2.29894E+31, &
  3.11276E+31,  4.18991E+31,  5.60888E+31,  7.45425E+31,  9.86136E+31, &
  1.29927E+32,  1.69848E+32,  2.21410E+32,  2.86927E+32,  3.70110E+32, &
  4.76257E+32,  6.08699E+32,  7.75028E+32,  9.83132E+32,  1.24253E+33, &
  1.56465E+33,  1.96310E+33,  2.45397E+33,  3.05653E+33,  3.79420E+33, &
  4.69507E+33,  5.79265E+33,  7.12686E+33,  8.74528E+33,  1.07047E+34, &
  1.30342E+34,  1.58622E+34,  1.92738E+34,  2.33377E+34,  2.82793E+34, &
  3.40161E+34,  4.07031E+34,  4.87101E+34,  5.80444E+34,  6.91310E+34, &
  8.20241E+34,  9.72756E+34,  1.14906E+35,  1.35771E+35,  1.59676E+35, &
  1.87701E+35,  2.20244E+35,  2.57647E+35,  3.01325E+35,  3.51471E+35, &
  4.08956E+35 &
         /)

         real(dp),  dimension(NsplinePts) :: ratesRMF_bcc_relaxed = (/ &
  6.11500E-10,  4.92931E-01,  3.05661E+04,  5.11765E+07,  1.29742E+10, &
  1.03907E+12,  3.84453E+13,  8.19329E+14,  1.15458E+16,  1.18213E+17, &
  9.38983E+17,  6.06234E+18,  3.29862E+19,  1.55351E+20,  6.46952E+20, &
  2.42496E+21,  8.27938E+21,  2.61225E+22,  7.65988E+22,  2.11118E+23, &
  5.49402E+23,  1.35787E+24,  3.20317E+24,  7.24997E+24,  1.57918E+25, &
  3.32018E+25,  6.76305E+25,  1.33589E+26,  2.56847E+26,  4.81624E+26, &
  8.81077E+26,  1.57751E+27,  2.76647E+27,  4.76129E+27,  8.03994E+27, &
  1.33608E+28,  2.18447E+28,  3.51472E+28,  5.57917E+28,  8.74763E+28, &
  1.35127E+29,  2.06372E+29,  3.11674E+29,  4.65597E+29,  6.88399E+29, &
  1.00819E+30,  1.46231E+30,  2.10051E+30,  2.99391E+30,  4.23156E+30, &
  5.93003E+30,  8.25113E+30,  1.14000E+31,  1.56409E+31,  2.13148E+31, &
  2.88634E+31,  3.88556E+31,  5.20200E+31,  6.91421E+31,  9.14787E+31, &
  1.20538E+32,  1.57590E+32,  2.05450E+32,  2.66270E+32,  3.43496E+32, &
  4.42050E+32,  5.65030E+32,  7.19489E+32,  9.12758E+32,  1.15369E+33, &
  1.45290E+33,  1.82303E+33,  2.27906E+33,  2.83890E+33,  3.52432E+33, &
  4.36144E+33,  5.38144E+33,  6.62144E+33,  8.12567E+33,  9.94694E+33, &
  1.21124E+34,  1.47415E+34,  1.79133E+34,  2.16918E+34,  2.62867E+34, &
  3.16214E+34,  3.78401E+34,  4.52868E+34,  5.39685E+34,  6.42807E+34, &
  7.62739E+34,  9.04617E+34,  1.06864E+35,  1.26276E+35,  1.48518E+35, &
  1.74594E+35,  2.04877E+35,  2.39684E+35,  2.80333E+35,  3.27003E+35, &
  3.80507E+35 &
         /)

         real(dp),  dimension(NsplinePts) :: ratesRMF_fcc_static = (/ &
  2.04401E-15,  6.52173E-06,  8.44904E-01,  2.31857E+03,  8.48894E+05, &
  9.08995E+07,  4.27074E+09,  1.11397E+11,  1.86920E+12,  2.23087E+13, &
  2.03098E+14,  1.48237E+15,  9.01522E+15,  4.70040E+16,  2.14950E+17, &
  8.78558E+17,  3.25101E+18,  1.10579E+19,  3.47909E+19,  1.02455E+20, &
  2.83814E+20,  7.44168E+20,  1.85667E+21,  4.43231E+21,  1.01570E+22, &
  2.24147E+22,  4.78217E+22,  9.87451E+22,  1.98105E+23,  3.86969E+23, &
  7.36297E+23,  1.36915E+24,  2.49033E+24,  4.43973E+24,  7.75657E+24, &
  1.33214E+25,  2.24858E+25,  3.73135E+25,  6.10316E+25,  9.85147E+25, &
  1.56537E+26,  2.45721E+26,  3.81140E+26,  5.84354E+26,  8.86122E+26, &
  1.33016E+27,  1.97622E+27,  2.90605E+27,  4.23793E+27,  6.12518E+27, &
  8.77315E+27,  1.24703E+28,  1.75925E+28,  2.46347E+28,  3.42484E+28, &
  4.72931E+28,  6.48963E+28,  8.85288E+28,  1.19851E+29,  1.61454E+29, &
  2.16537E+29,  2.88050E+29,  3.81978E+29,  5.03398E+29,  6.60141E+29, &
  8.63347E+29,  1.12114E+30,  1.45001E+30,  1.86787E+30,  2.39668E+30, &
  3.06323E+30,  3.89995E+30,  4.94581E+30,  6.24814E+30,  7.86499E+30, &
  9.86696E+30,  1.23393E+31,  1.53850E+31,  1.91280E+31,  2.37183E+31, &
  2.92501E+31,  3.60463E+31,  4.43449E+31,  5.43546E+31,  6.66618E+31, &
  8.11429E+31,  9.82385E+31,  1.18931E+32,  1.43347E+32,  1.72660E+32, &
  2.07151E+32,  2.48379E+32,  2.96593E+32,  3.54218E+32,  4.21010E+32, &
  5.00095E+32,  5.92883E+32,  7.00674E+32,  8.27749E+32,  9.75157E+32, &
  1.14586E+33 &
         /)

         real(dp),  dimension(NsplinePts) :: ratesRMF_fcc_ws = (/ &
  1.98357E-10,  1.94537E-01,  1.33989E+04,  2.40705E+07,  6.43057E+09, &
  5.36774E+11,  2.05485E+13,  4.50716E+14,  6.51142E+15,  6.81405E+16, &
  5.51878E+17,  3.62592E+18,  2.00446E+19,  9.57804E+19,  4.04230E+20, &
  1.53399E+21,  5.29782E+21,  1.68953E+22,  5.00418E+22,  1.39231E+23, &
  3.65569E+23,  9.11163E+23,  2.16665E+24,  4.94130E+24,  1.08412E+25, &
  2.29513E+25,  4.70603E+25,  9.35467E+25,  1.80953E+26,  3.41294E+26, &
  6.27868E+26,  1.13024E+27,  1.99243E+27,  3.44638E+27,  5.84788E+27, &
  9.76374E+27,  1.60363E+28,  2.59155E+28,  4.13137E+28,  6.50450E+28, &
  1.00882E+29,  1.54675E+29,  2.34489E+29,  3.51593E+29,  5.21719E+29, &
  7.66773E+29,  1.11596E+30,  1.60838E+30,  2.29994E+30,  3.26108E+30, &
  4.58427E+30,  6.39804E+30,  8.86604E+30,  1.21997E+31,  1.66727E+31, &
  2.26403E+31,  3.05614E+31,  4.10253E+31,  5.46717E+31,  7.25196E+31, &
  9.57979E+31,  1.25554E+32,  1.64083E+32,  2.13164E+32,  2.75632E+32, &
  3.55530E+32,  4.55467E+32,  5.81263E+32,  7.39012E+32,  9.36083E+32, &
  1.18134E+33,  1.48538E+33,  1.86074E+33,  2.32248E+33,  2.88892E+33, &
  3.58208E+33,  4.42829E+33,  5.45895E+33,  6.71157E+33,  8.23096E+33, &
  1.00410E+34,  1.22422E+34,  1.49024E+34,  1.80771E+34,  2.19437E+34, &
  2.64414E+34,  3.16940E+34,  3.79934E+34,  4.53502E+34,  5.41019E+34, &
  6.42973E+34,  7.63760E+34,  9.03630E+34,  1.06939E+35,  1.25964E+35, &
  1.48300E+35,  1.74276E+35,  2.04179E+35,  2.39148E+35,  2.79355E+35, &
  3.25517E+35 &
         /)

         real(dp),  dimension(NsplinePts) :: ratesRMF_fcc_relaxed = (/ &
  2.75978E-10,  2.59176E-01,  1.74411E+04,  3.08479E+07,  8.14624E+09, &
  6.73787E+11,  2.56000E+13,  5.57953E+14,  8.01641E+15,  8.34854E+16, &
  6.73257E+17,  4.40633E+18,  2.42736E+19,  1.15616E+20,  4.86509E+20, &
  1.84119E+21,  6.34266E+21,  2.01796E+22,  5.96368E+22,  1.65581E+23, &
  4.33898E+23,  1.07946E+24,  2.56230E+24,  5.83384E+24,  1.27790E+25, &
  2.70124E+25,  5.53065E+25,  1.09785E+26,  2.12079E+26,  3.99486E+26, &
  7.34011E+26,  1.31973E+27,  2.32380E+27,  4.01510E+27,  6.80559E+27, &
  1.13510E+28,  1.86245E+28,  3.00689E+28,  4.78896E+28,  7.53292E+28, &
  1.16728E+29,  1.78816E+29,  2.70860E+29,  4.05795E+29,  6.01667E+29, &
  8.83586E+29,  1.28500E+30,  1.85063E+30,  2.64445E+30,  3.74692E+30, &
  5.26361E+30,  7.34123E+30,  1.01664E+31,  1.39800E+31,  1.90936E+31, &
  2.59118E+31,  3.49564E+31,  4.68974E+31,  6.24608E+31,  8.28045E+31, &
  1.09323E+32,  1.43202E+32,  1.87047E+32,  2.42869E+32,  3.13879E+32, &
  4.04658E+32,  5.18145E+32,  6.60929E+32,  8.39892E+32,  1.06336E+33, &
  1.34135E+33,  1.68579E+33,  2.11083E+33,  2.63347E+33,  3.27432E+33, &
  4.05821E+33,  5.01477E+33,  6.17935E+33,  7.59415E+33,  9.30956E+33, &
  1.13522E+34,  1.38354E+34,  1.68354E+34,  2.04140E+34,  2.47711E+34, &
  2.98373E+34,  3.57513E+34,  4.28416E+34,  5.11189E+34,  6.09623E+34, &
  7.24253E+34,  8.60013E+34,  1.01716E+35,  1.20335E+35,  1.41696E+35, &
  1.66767E+35,  1.95915E+35,  2.29457E+35,  2.68670E+35,  3.13743E+35, &
  3.65474E+35 &
         /)


         real(dp),  dimension(NsplinePts) :: derivs2RMF_SPVH = (/ &
  1.40949E-11, -2.81899E-11,  9.86645E-11, -3.66463E-10,  1.36878E-09, &
 -4.95547E-09,  2.49415E-08,  5.78367E-08,  2.05159E-06,  1.65361E-05, &
  1.35052E-04,  7.78794E-04,  4.07075E-03,  1.73804E-02,  6.87726E-02, &
  2.34790E-01,  7.60765E-01,  2.20588E+00,  6.10568E+00,  1.56965E+01, &
  3.83494E+01,  8.88595E+01,  1.97874E+02,  4.23514E+02,  8.66755E+02, &
  1.73987E+03,  3.33236E+03,  6.26065E+03,  1.15454E+04,  2.03240E+04, &
  3.58855E+04,  6.07566E+04,  1.03232E+05,  1.66942E+05,  2.74498E+05, &
  4.35496E+05,  6.77134E+05,  1.04970E+06,  1.64388E+06,  2.37394E+06, &
  3.63015E+06,  5.32259E+06,  7.75038E+06,  1.11667E+07,  1.60440E+07, &
  2.26902E+07,  3.12806E+07,  4.45968E+07,  6.10118E+07,  8.23971E+07, &
  1.13864E+08,  1.53253E+08,  2.05321E+08,  2.72839E+08,  3.62906E+08, &
  4.75741E+08,  6.39288E+08,  7.86221E+08,  1.05249E+09,  1.43866E+09, &
  1.58464E+09,  2.36882E+09,  2.74183E+09,  3.46819E+09,  4.87243E+09, &
  5.19520E+09,  7.15785E+09,  8.74401E+09,  1.08995E+10,  1.34581E+10, &
  1.65570E+10,  2.02454E+10,  2.46974E+10,  3.01598E+10,  3.68079E+10, &
  4.46662E+10,  5.45929E+10,  6.47109E+10,  8.36264E+10,  8.11857E+10, &
  1.21606E+11,  1.44505E+11,  1.42597E+11,  2.38544E+11,  1.84029E+11, &
  2.21153E+11,  3.66275E+11,  2.92984E+11,  4.92915E+11,  4.08186E+11, &
  6.78765E+11,  5.28169E+11,  9.62843E+11,  6.90142E+11,  1.16218E+12, &
  1.23044E+12,  1.20199E+12,  1.89448E+12,  1.42823E+12,  3.07985E+12, &
 -1.53992E+12 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2RMF_bcc_static = (/ &
  5.79601E-10, -1.15920E-09,  4.05721E-09, -1.50694E-08,  5.62849E-08, &
 -2.03869E-07,  1.02249E-06,  2.32156E-06,  8.37181E-05,  6.74851E-04, &
  5.52297E-03,  3.18836E-02,  1.66873E-01,  7.13202E-01,  2.82494E+00, &
  9.65300E+00,  3.13039E+01,  9.08388E+01,  2.51615E+02,  6.47302E+02, &
  1.58249E+03,  3.66904E+03,  8.17496E+03,  1.75067E+04,  3.58475E+04, &
  7.19935E+04,  1.37955E+05,  2.59298E+05,  4.78384E+05,  8.42490E+05, &
  1.48814E+06,  2.52051E+06,  4.28417E+06,  6.93081E+06,  1.13998E+07, &
  1.80923E+07,  2.81403E+07,  4.36367E+07,  6.83568E+07,  9.87484E+07, &
  1.51040E+08,  2.21522E+08,  3.22653E+08,  4.64999E+08,  6.68267E+08, &
  9.45338E+08,  1.30358E+09,  1.85890E+09,  2.54375E+09,  3.43618E+09, &
  4.74937E+09,  6.39375E+09,  8.56789E+09,  1.13878E+10,  1.51500E+10, &
  1.98644E+10,  2.66978E+10,  3.28426E+10,  4.39709E+10,  6.01124E+10, &
  6.62360E+10,  9.90107E+10,  1.14637E+11,  1.45027E+11,  2.03758E+11, &
  2.17336E+11,  2.99447E+11,  3.65873E+11,  4.56132E+11,  5.63294E+11, &
  6.93105E+11,  8.47634E+11,  1.03418E+12,  1.26309E+12,  1.54173E+12, &
  1.87113E+12,  2.28727E+12,  2.71158E+12,  3.50441E+12,  3.40348E+12, &
  5.09707E+12,  6.05786E+12,  5.97993E+12,  1.00009E+13,  7.72073E+12, &
  9.27825E+12,  1.53623E+13,  1.22958E+13,  2.06790E+13,  1.71340E+13, &
  2.84822E+13,  2.21770E+13,  4.04093E+13,  2.89863E+13,  4.87901E+13, &
  5.16688E+13,  5.04877E+13,  7.95591E+13,  5.99990E+13,  1.29391E+14, &
 -6.46955E+13 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2RMF_bcc_ws = (/ &
  2.18015E-07, -4.36031E-07,  1.52611E-06, -5.66805E-06,  2.12326E-05, &
 -7.24628E-05,  5.12442E-04,  2.99972E-03,  5.39991E-02,  4.22020E-01, &
  3.02532E+00,  1.61802E+01,  7.75946E+01,  3.09350E+02,  1.14383E+03, &
  3.67745E+03,  1.12653E+04,  3.09833E+04,  8.17130E+04,  2.00581E+05, &
  4.69360E+05,  1.04386E+06,  2.23720E+06,  4.61524E+06,  9.11959E+06, &
  1.77168E+07,  3.28442E+07,  5.98895E+07,  1.07292E+08,  1.83500E+08, &
  3.15662E+08,  5.20464E+08,  8.63222E+08,  1.36089E+09,  2.18940E+09, &
  3.39409E+09,  5.15962E+09,  7.83797E+09,  1.20427E+10,  1.69886E+10, &
  2.55537E+10,  3.67475E+10,  5.25411E+10,  7.43664E+10,  1.05075E+11, &
  1.46069E+11,  1.97857E+11,  2.78239E+11,  3.74314E+11,  4.97408E+11, &
  6.78275E+11,  8.99339E+11,  1.18774E+12,  1.55628E+12,  2.04268E+12, &
  2.64190E+12,  3.51060E+12,  4.24071E+12,  5.63014E+12,  7.62795E+12, &
  8.19402E+12,  1.22872E+13,  1.39249E+13,  1.74461E+13,  2.44355E+13, &
  2.53923E+13,  3.49708E+13,  4.21762E+13,  5.20580E+13,  6.36132E+13, &
  7.74688E+13,  9.37634E+13,  1.13256E+14,  1.37019E+14,  1.65708E+14, &
  1.99262E+14,  2.41495E+14,  2.83374E+14,  3.64851E+14,  3.44484E+14, &
  5.23142E+14,  6.14506E+14,  5.92042E+14,  1.01000E+15,  7.41523E+14, &
  8.91739E+14,  1.50837E+15,  1.15538E+15,  1.99587E+15,  1.58864E+15, &
  2.70863E+15,  2.01429E+15,  3.80008E+15,  2.58080E+15,  4.49155E+15, &
  4.67234E+15,  4.47932E+15,  7.16446E+15,  5.27531E+15,  1.13294E+16, &
 -5.66471E+15 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2RMF_bcc_relaxed = (/ &
  2.18015E-07, -4.36031E-07,  1.52611E-06, -5.66805E-06,  2.12326E-05, &
 -7.24628E-05,  5.12442E-04,  2.99972E-03,  5.39991E-02,  4.22020E-01, &
  3.02532E+00,  1.61802E+01,  7.75946E+01,  3.09350E+02,  1.14383E+03, &
  3.67745E+03,  1.12653E+04,  3.09833E+04,  8.17130E+04,  2.00581E+05, &
  4.69360E+05,  1.04386E+06,  2.23720E+06,  4.61524E+06,  9.11959E+06, &
  1.77168E+07,  3.28442E+07,  5.98895E+07,  1.07292E+08,  1.83500E+08, &
  3.15662E+08,  5.20464E+08,  8.63222E+08,  1.36089E+09,  2.18940E+09, &
  3.39409E+09,  5.15962E+09,  7.83797E+09,  1.20427E+10,  1.69886E+10, &
  2.55537E+10,  3.67475E+10,  5.25411E+10,  7.43664E+10,  1.05075E+11, &
  1.46069E+11,  1.97857E+11,  2.78239E+11,  3.74314E+11,  4.97408E+11, &
  6.78275E+11,  8.99339E+11,  1.18774E+12,  1.55628E+12,  2.04268E+12, &
  2.64190E+12,  3.51060E+12,  4.24071E+12,  5.63014E+12,  7.62795E+12, &
  8.19402E+12,  1.22872E+13,  1.39249E+13,  1.74461E+13,  2.44355E+13, &
  2.53923E+13,  3.49708E+13,  4.21762E+13,  5.20580E+13,  6.36132E+13, &
  7.74688E+13,  9.37634E+13,  1.13256E+14,  1.37019E+14,  1.65708E+14, &
  1.99262E+14,  2.41495E+14,  2.83374E+14,  3.64851E+14,  3.44484E+14, &
  5.23142E+14,  6.14506E+14,  5.92042E+14,  1.01000E+15,  7.41523E+14, &
  8.91739E+14,  1.50837E+15,  1.15538E+15,  1.99587E+15,  1.58864E+15, &
  2.70863E+15,  2.01429E+15,  3.80008E+15,  2.58080E+15,  4.49155E+15, &
  4.67234E+15,  4.47932E+15,  7.16446E+15,  5.27531E+15,  1.13294E+16, &
 -5.66471E+15 &
         /)
 
         real(dp),  dimension(NsplinePts) :: derivs2RMF_fcc_static = (/ &
  8.30187E-11, -1.66037E-10,  5.81131E-10, -2.15847E-09,  8.05792E-09, &
 -2.95271E-08,  1.35088E-07,  1.19397E-07,  9.49247E-06,  7.62768E-05, &
  6.67032E-04,  3.98030E-03,  2.16951E-02,  9.56852E-02,  3.91138E-01, &
  1.37412E+00,  4.57362E+00,  1.36000E+01,  3.85236E+01,  1.01246E+02, &
  2.52512E+02,  5.96666E+02,  1.35318E+03,  2.94761E+03,  6.13444E+03, &
  1.25081E+04,  2.43318E+04,  4.63708E+04,  8.67045E+04,  1.54742E+05, &
  2.76657E+05,  4.74346E+05,  8.15342E+05,  1.33458E+06,  2.21751E+06, &
  3.55707E+06,  5.59045E+06,  8.75097E+06,  1.38306E+07,  2.01941E+07, &
  3.11281E+07,  4.60636E+07,  6.76615E+07,  9.83171E+07,  1.42396E+08, &
  2.03038E+08,  2.82255E+08,  4.05096E+08,  5.58631E+08,  7.60253E+08, &
  1.05735E+09,  1.43331E+09,  1.93346E+09,  2.58655E+09,  3.46234E+09, &
  4.56815E+09,  6.17192E+09,  7.65400E+09,  1.02887E+10,  1.41252E+10, &
  1.57366E+10,  2.35050E+10,  2.74701E+10,  3.49100E+10,  4.91322E+10, &
  5.29993E+10,  7.30719E+10,  8.98025E+10,  1.12470E+11,  1.39562E+11, &
  1.72533E+11,  2.11996E+11,  2.59837E+11,  3.18728E+11,  3.90682E+11, &
  4.76160E+11,  5.84355E+11,  6.95973E+11,  9.01193E+11,  8.85806E+11, &
  1.31951E+12,  1.57640E+12,  1.57239E+12,  2.60889E+12,  2.05686E+12, &
  2.47237E+12,  4.05886E+12,  3.30909E+12,  5.50622E+12,  4.63985E+12, &
  7.63537E+12,  6.06033E+12,  1.08895E+13,  7.99093E+12,  1.32733E+13, &
  1.41651E+13,  1.39544E+13,  2.18623E+13,  1.66537E+13,  3.59940E+13, &
 -1.79970E+13 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2RMF_fcc_ws = (/ &
  1.29400E-07, -2.58800E-07,  9.05799E-07, -3.36425E-06,  1.25903E-05, &
 -4.37894E-05,  2.81829E-04,  1.42737E-03,  2.84778E-02,  2.24841E-01, &
  1.65623E+00,  9.00761E+00,  4.40072E+01,  1.78092E+02,  6.68260E+02, &
  2.17676E+03,  6.75002E+03,  1.87795E+04,  5.00508E+04,  1.24102E+05, &
  2.93138E+05,  6.57781E+05,  1.42155E+06,  2.95617E+06,  5.88612E+06, &
  1.15167E+07,  2.15024E+07,  3.94640E+07,  7.11461E+07,  1.22448E+08, &
  2.11835E+08,  3.51296E+08,  5.85711E+08,  9.28529E+08,  1.50090E+09, &
  2.33851E+09,  3.57247E+09,  5.45088E+09,  8.40977E+09,  1.19247E+10, &
  1.80006E+10,  2.59955E+10,  3.73160E+10,  5.30217E+10,  7.51889E+10, &
  1.04916E+11,  1.42660E+11,  2.01213E+11,  2.71686E+11,  3.62306E+11, &
  4.95472E+11,  6.59106E+11,  8.73190E+11,  1.14764E+12,  1.51068E+12, &
  1.95959E+12,  2.61024E+12,  3.16561E+12,  4.21015E+12,  5.71492E+12, &
  6.17353E+12,  9.24947E+12,  1.05318E+13,  1.32221E+13,  1.85299E+13, &
  1.93669E+13,  2.66715E+13,  3.22576E+13,  3.99007E+13,  4.88680E+13, &
  5.96439E+13,  7.23502E+13,  8.75794E+13,  1.06171E+14,  1.28655E+14, &
  1.55013E+14,  1.88212E+14,  2.21340E+14,  2.85195E+14,  2.70972E+14, &
  4.10165E+14,  4.83030E+14,  4.67889E+14,  7.94569E+14,  5.89977E+14, &
  7.09275E+14,  1.19385E+15,  9.23531E+14,  1.58554E+15,  1.27344E+15, &
  2.15859E+15,  1.62192E+15,  3.03559E+15,  2.08717E+15,  3.60457E+15, &
  3.76450E+15,  3.62431E+15,  5.77689E+15,  4.27650E+15,  9.19169E+15, &
 -4.59584E+15 &
         /)

         real(dp),  dimension(NsplinePts) :: derivs2RMF_fcc_relaxed = (/ &
  1.54648E-07, -3.09296E-07,  1.08254E-06, -4.02066E-06,  1.50496E-05, &
 -5.21525E-05,  3.42080E-04,  1.79021E-03,  3.48975E-02,  2.74968E-01, &
  2.01370E+00,  1.09130E+01,  5.31052E+01,  2.14227E+02,  8.01337E+02, &
  2.60299E+03,  8.05081E+03,  2.23437E+04,  5.94171E+04,  1.47011E+05, &
  3.46557E+05,  7.76175E+05,  1.67444E+06,  3.47615E+06,  6.91022E+06, &
  1.35000E+07,  2.51673E+07,  4.61263E+07,  8.30458E+07,  1.42737E+08, &
  2.46638E+08,  4.08509E+08,  6.80341E+08,  1.07727E+09,  1.73958E+09, &
  2.70749E+09,  4.13183E+09,  6.29844E+09,  9.70886E+09,  1.37517E+10, &
  2.07428E+10,  2.99287E+10,  4.29257E+10,  6.09423E+10,  8.63541E+10, &
  1.20399E+11,  1.63580E+11,  2.30574E+11,  3.11088E+11,  4.14539E+11, &
  5.66556E+11,  7.53142E+11,  9.97107E+11,  1.30965E+12,  1.72289E+12, &
  2.23346E+12,  2.97350E+12,  3.60314E+12,  4.79024E+12,  6.49971E+12, &
  7.01299E+12,  1.05090E+13,  1.19540E+13,  1.50011E+13,  2.10203E+13, &
  2.19431E+13,  3.02193E+13,  3.65267E+13,  4.51608E+13,  5.52835E+13, &
  6.74423E+13,  8.17713E+13,  9.89381E+13,  1.19889E+14,  1.45217E+14, &
  1.74894E+14,  2.12268E+14,  2.49514E+14,  3.21444E+14,  3.05009E+14, &
  4.62000E+14,  5.43780E+14,  5.26134E+14,  8.94336E+14,  6.62479E+14, &
  7.96483E+14,  1.34203E+15,  1.03600E+15,  1.78092E+15,  1.42766E+15, &
  2.42296E+15,  1.81661E+15,  3.40564E+15,  2.33554E+15,  4.04002E+15, &
  4.21576E+15,  4.05513E+15,  6.46826E+15,  4.78290E+15,  1.02783E+16, &
 -5.13916E+15 &
         /)

         n = size(densities)
         h = 0.0_dp
        
         klo=max(min(locate(densities,rho),n-1),1)
        
         khi=klo+1
         h=densities(khi)-densities(klo)
	
         if ((h .eq. 0.0) .or. (rho .lt. 1.00000E+09 )) then
            ! print *,'bad density input in M3Y_Rate'
            RMF_Rate = 0.0_dp
         else
            a=(densities(khi)-rho)/h
            b=(rho-densities(klo))/h

            select case  (pycnoType)
               case (0)
                  ratelo = ratesRMF_SPVH(klo)
                  ratehi = ratesRMF_SPVH(khi)
                  derivs2lo = derivs2RMF_SPVH(klo)
                  derivs2hi = derivs2RMF_SPVH(khi)
               case (1)
                  ratelo = ratesRMF_bcc_static(klo)
                  ratehi = ratesRMF_bcc_static(khi)
                  derivs2lo = derivs2RMF_bcc_static(klo)
                  derivs2hi = derivs2RMF_bcc_static(khi)
               case (2)
                  ratelo = ratesRMF_bcc_ws(klo)
                  ratehi = ratesRMF_bcc_ws(khi)
                  derivs2lo = derivs2RMF_bcc_ws(klo)
                  derivs2hi = derivs2RMF_bcc_ws(khi)
               case (3)
                  ratelo = ratesRMF_bcc_relaxed(klo)
                  ratehi = ratesRMF_bcc_relaxed(khi)
                  derivs2lo = derivs2RMF_bcc_relaxed(klo)
                  derivs2hi = derivs2RMF_bcc_relaxed(khi)
               case (4)
                  ratelo = ratesRMF_fcc_static(klo)
                  ratehi = ratesRMF_fcc_static(khi)
                  derivs2lo = derivs2RMF_fcc_static(klo)
                  derivs2hi = derivs2RMF_fcc_static(khi)
               case (5)
                  ratelo = ratesRMF_fcc_ws(klo)
                  ratehi = ratesRMF_fcc_ws(khi)
                  derivs2lo = derivs2RMF_fcc_ws(klo)
                  derivs2hi = derivs2RMF_fcc_ws(khi)
               case (6)
                  ratelo = ratesRMF_fcc_relaxed(klo)
                  ratehi = ratesRMF_fcc_relaxed(khi)
                  derivs2lo = derivs2RMF_fcc_relaxed(klo)
                  derivs2hi = derivs2RMF_fcc_relaxed(khi)
            end select

            RMF_Rate = a*ratelo+b*ratehi+((a**3.0_dp-a)*derivs2lo+ & 
                        (b**3.0_dp-b)*derivs2hi)*(h**2.0_dp)/6.0_dp
         end if
    
          
      end function RMF_Rate

      real(kind=dp) function ndiff5pt(f1, f2, f3, f4, h)

         real(kind=dp), intent(in)   :: f1, f2, f3, f4, h
         ndiff5pt = (f1 - 8.0_dp*f2 + 8.0_dp*f3 - f4)/(h*12.0_dp)
         
      end function ndiff5pt

      end module pycno
      
