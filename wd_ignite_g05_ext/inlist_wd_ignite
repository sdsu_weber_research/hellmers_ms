
! inlist_wd_ignition


&star_job
      show_log_description_at_start = .false.
      
      load_saved_model = .true.
      !saved_model_name = 'wd_ignite.mod'
      saved_model_name = '../startmodels/start.mod' ! this is just wd_ignite run for 10 steps with PC shut off

      save_model_when_terminate = .true.
      save_model_filename = 'final.mod'

      change_initial_net = .true.      
      new_net_name = 'approx21.net'
      
      set_rate_3a = 'FL87' ! Fushiki and Lamb, Apj, 317, 368-388, 1987
      set_rate_1212 = 'G05' ! Gasques, et al. Phys Review C, 72, 025806, 2005

      show_log_description_at_start = .false. 

      set_HELM_ion_neutral_blends = .true.
      max_logRho_neutral_HELM = -20

      change_v_flag = .true.
      new_v_flag = .true.

      change_conv_vel_flag = .true.
      new_conv_vel_flag = .true.

      ! move the surface down to larger optical depth
      set_tau_factor = .true.
      set_to_this_tau_factor = 300 


      !pgstar_flag = .true.
      
/ ! end of star_job namelist


&controls

      use_gold_tolerances = .true.
      use_eosDT2 = .true.
      use_eosELM = .false.

      use_dedt_form_of_energy_eqn = .false.
      use_eps_mdot = .false.

      ! check for retries and backups as part of test_suite
         max_number_backups = 14
         max_number_retries = 120
         max_model_number = 600

      initial_mass =  0.45
      initial_z = 0.02d0
      
      use_Type2_opacities = .true.
      Zbase = 0.02d0
      
      power_nuc_burn_upper_limit = 1d8 
      
      log_center_density_limit = 15
      
      varcontrol_target = 1d-3
      mesh_delta_coeff = 0.5
      
      logQ_limit = 1d6
      gamma_center_limit = 1d6
      
      min_timestep_limit = 1d-4 ! seconds

      delta_lgL_nuc_limit = 0.05 ! limit for magnitude of change in lgL_nuc

      T_mix_limit = 1d4
      
      which_atm_option = 'grey_and_kap'

      accrete_same_as_surface = .false. 
      accrete_given_mass_fractions = .true. 
      num_accretion_species = 2
      accretion_species_id(1) = 'c12'
      accretion_species_xa(1) = 0.25
      accretion_species_id(2) = 'o16'
      accretion_species_xa(2) = 0.75
      
      mass_change = 1d-9 ! rate of accretion (Msun/year)

      num_trace_history_values = 2
      trace_history_value_name(1) = 'rel_E_err'
      trace_history_value_name(2) = 'log_rel_run_E_err'

      photo_interval = 50
      profile_interval = 1
      history_interval = 1
      terminal_interval = 5
      write_header_frequency = 10
      max_num_profile_models = 500

      !photo_interval = 1
      !profile_interval = 1
      !history_interval = 1
      !terminal_interval = 1

! FOR DEBUGGING

      !report_hydro_solver_progress = .true. ! set true to see info about newton iterations
      !report_ierr = .true. ! if true, produce terminal output when have some internal error
      !stop_for_bad_nums = .true.

      ! hydro debugging
      !hydro_check_everything = .true.
      !hydro_inspectB_flag = .true.
      !hydro_sizequ_flag = .true.
      
      ! for making logs, uncomment hydro_dump_call_number plus the following
      ! to make residual logs, uncomment hydro_sizequ_flag
      ! to make correction logs, uncomment hydro_inspectB_flag
      ! to make jacobian logs, uncomment hydro_numerical_jacobian, hydro_save_numjac_plot_data
      ! to do dfridr test, uncomment hydro_get_a_numerical_partial, hydro_test_partials_k,
      !     hydro_numerical_jacobian, hydro_save_numjac_plot_data, hydro_dump_iter_number
         
      !hydro_get_a_numerical_partial = 1d-4
      !hydro_test_partials_k = 1
      !hydro_numerical_jacobian = .true.
      !hydro_save_numjac_plot_data = .true.
      !hydro_dump_call_number = 195
      !hydro_dump_iter_number = 5
      !hydro_epsder_struct = 1d-6
      !hydro_epsder_chem = 1d-6
      !hydro_save_photo = .true. ! Saves a photo when hydro_call_number = hydro_dump_call_number -1

      !fill_arrays_with_NaNs = .true.
      
      !max_years_for_timestep = 3.67628942044319d-05

      !report_why_dt_limits = .true.
      !report_all_dt_limits = .true.
      !report_hydro_dt_info = .true.
      !report_dX_nuc_drop_dt_limits = .true.
      !report_bad_negative_xa = .true.
      
      !show_mesh_changes = .true.
      !mesh_dump_call_number = 5189
      !okay_to_remesh = .false.
      
      !trace_evolve = .true.

      !trace_newton_bcyclic_solve_input = .true. ! input is "B" j k iter B(j,k)
      !trace_newton_bcyclic_solve_output = .true. ! output is "X" j k iter X(j,k)

      !trace_newton_bcyclic_matrix_input = .true.
      !trace_newton_bcyclic_matrix_output = .true.
      
      !trace_newton_bcyclic_steplo = 1 ! 1st model number to trace
      !trace_newton_bcyclic_stephi = 1 ! last model number to trace
      
      !trace_newton_bcyclic_iterlo = 2 ! 1st newton iter to trace
      !trace_newton_bcyclic_iterhi = 2 ! last newton iter to trace
      
      !trace_newton_bcyclic_nzlo = 1 ! 1st cell to trace
      !trace_newton_bcyclic_nzhi = 10000 ! last cell to trace; if < 0, then use nz as nzhi
      
      !trace_newton_bcyclic_jlo = 1 ! 1st var to trace
      !trace_newton_bcyclic_jhi = 100 ! last var to trace; if < 0, then use nvar as jhi
      
      !trace_k = 0

/ ! end of controls namelist



&pgstar

!pause = .true.

         TRho_Profile_win_flag = .true.
         TRho_Profile_xmin = -2.5
         TRho_Profile_xmax = 11.0
         TRho_Profile_ymin = 5.1
         TRho_Profile_ymax = 10.0        
         
         !History_Panels1_win_flag = .true.
         History_Panels1_yaxis_name(3) = 'log_R' 
         History_Panels1_other_yaxis_name(3) = 'v_div_csound_surf' 

/ ! end of pgstar namelist
