#!/bin/bash
cd ~/bitbucket/hellmers_ms
cd wd_ignite_nopycno;./clean;./clroutput;./mk;./rn;cd ..
cd wd_ignite_g05;./clean;./clroutput;./mk;./rn;cd ..

cd wd_ignite_m3y_spvh;./clean;./clroutput;./mk;./rn;cd ..
cd wd_ignite_m3y_bcc_relaxed;./clean;./clroutput;./mk;./rn;cd ..
cd wd_ignite_m3y_bcc_static;./clean;./clroutput;./mk;./rn;cd ..
cd wd_ignite_m3y_bcc_ws;./clean;./clroutput;./mk;./rn;cd ..
cd wd_ignite_m3y_fcc_relaxed;./clean;./clroutput;./mk;./rn;cd ..
cd wd_ignite_m3y_fcc_static;./clean;./clroutput;./mk;./rn;cd ..
cd wd_ignite_m3y_fcc_ws;./clean;./clroutput;./mk;./rn;cd ..

cd wd_ignite_sp_spvh;./clean;./clroutput;./mk;./rn;cd ..
cd wd_ignite_sp_bcc_relaxed;./clean;./clroutput;./mk;./rn;cd ..
cd wd_ignite_sp_bcc_static;./clean;./clroutput;./mk;./rn;cd ..
cd wd_ignite_sp_bcc_ws;./clean;./clroutput;./mk;./rn;cd ..
cd wd_ignite_sp_fcc_relaxed;./clean;./clroutput;./mk;./rn;cd ..
cd wd_ignite_sp_fcc_static;./clean;./clroutput;./mk;./rn;cd ..
cd wd_ignite_sp_fcc_ws;./clean;./clroutput;./mk;./rn;cd ..

