#!/bin/bash
cd ~/bitbucket/hellmers_ms
cd wd_ignite_nopycno;./clean;rm -f photos/*;cd ..
cd wd_ignite_g05;./clean;rm -f photos/*;cd ..

cd wd_ignite_m3y_spvh;./clean;rm -f photos/*;cd ..
cd wd_ignite_m3y_bcc_relaxed;./clean;rm -f photos/*;cd ..
cd wd_ignite_m3y_bcc_static;./clean;rm -f photos/*;cd ..
cd wd_ignite_m3y_bcc_ws;./clean;rm -f photos/*;cd ..
cd wd_ignite_m3y_fcc_relaxed;./clean;rm -f photos/*;cd ..
cd wd_ignite_m3y_fcc_static;./clean;rm -f photos/*;cd ..
cd wd_ignite_m3y_fcc_ws;./clean;rm -f photos/*;cd ..

cd wd_ignite_sp_spvh;./clean;rm -f photos/*;cd ..
cd wd_ignite_sp_bcc_relaxed;./clean;rm -f photos/*;cd ..
cd wd_ignite_sp_bcc_static;./clean;rm -f photos/*;cd ..
cd wd_ignite_sp_bcc_ws;./clean;rm -f photos/*;cd ..
cd wd_ignite_sp_fcc_relaxed;./clean;rm -f photos/*;cd ..
cd wd_ignite_sp_fcc_static;./clean;rm -f photos/*;cd ..
cd wd_ignite_sp_fcc_ws;./clean;rm -f photos/*;cd ..

cd wd_ignite_m3y_bcc_ws_ext;./clean;rm -f photos/*;cd ..
cd wd_ignite_m3y_bcc_relaxed_ext;./clean;rm -f photos/*;cd ..
cd wd_ignite_m3y_fcc_ws_ext;./clean;rm -f photos/*;cd ..
cd wd_ignite_m3y_fcc_relaxed_ext;./clean;rm -f photos/*;cd ..
cd wd_ignite_sp_bcc_ws_ext;./clean;rm -f photos/*;cd ..
cd wd_ignite_sp_bcc_relaxed_ext;./clean;rm -f photos/*;cd ..
cd wd_ignite_sp_fcc_ws_ext;./clean;rm -f photos/*;cd ..
cd wd_ignite_sp_fcc_relaxed_ext;./clean;rm -f photos/*;cd ..

