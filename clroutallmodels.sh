#!/bin/bash
input="modelslist.txt"
while IFS= read -r line
do
	echo "Cleaning model: $line"
	cd $line;./clean;./clroutput;rm -rf photos/*; cd ..
done < "$input"

